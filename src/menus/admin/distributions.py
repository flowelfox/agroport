import datetime
import enum

from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import to_state, unknown_command, group_buttons, get_settings
from botmanlib.menus.redy_to_use.instant_distribution import InstantDistributionMenu
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.models import DBSession, Event, User, Ticket, Subevent, Category
from src.settings import SETTINGS_FILE


class DistributionsMenu(BaseMenu):
    menu_name = 'distribution_menu'

    def entry(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        if not user.has_permission('distribution_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return self.States.ACTION

        self.send_message(context)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(_("Instant distribution"), callback_data='instant_distribution')],
                   [InlineKeyboardButton(_("Distribution by event"), callback_data='distribution_by_event')],
                   [InlineKeyboardButton(_("Distribution by subevent"), callback_data='distribution_by_subevent')],
                   [InlineKeyboardButton(_("Distribution by category"), callback_data='distribution_by_category')],
                   [InlineKeyboardButton(_("Distribution by region"), callback_data='distribution_by_region')],
                   [InlineKeyboardButton(_("Back"), callback_data=f'back_{self.menu_name}')]]

        self.send_or_edit(context, chat_id=user.chat_id, text=_("Choose distribution type:"), reply_markup=InlineKeyboardMarkup(buttons))

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        instant_distribution_menu = AgroportInstantDistributionMenu(User, self)
        distrubution_by_event = AgroportInstantDistributionByEventMenu(User, self)
        distrubution_by_subevent = AgroportInstantDistributionBySubeventMenu(User, self)
        distrubution_by_category = AgroportInstantDistributionByCategoryMenu(User, self)
        distrubution_by_region = AgroportInstantDistributionByRegionMenu(User, self)

        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^distribution$')],
                                      states={
                                          self.States.ACTION: [CallbackQueryHandler(self.back, pattern=f'^back_{self.menu_name}$'),
                                                               instant_distribution_menu.handler,
                                                               distrubution_by_event.handler,
                                                               distrubution_by_subevent.handler,
                                                               distrubution_by_category.handler,
                                                               distrubution_by_region.handler,
                                                               MessageHandler(Filters.all, to_state(self.States.ACTION))],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler


class AgroportInstantDistributionMenu(InstantDistributionMenu):
    menu_name = 'instant_distribution'

    def available_buttons(self, context):
        _ = context.user_data['_']
        buttons = [{"data": "start", "name": _("Return user to start menu")},
                   {"data": "my_tickets", "name": _("Moves user to My tickets menu")}]
        events = DBSession.query(Event).join(Event.subevents).filter(Event.end_date > datetime.datetime.now() + datetime.timedelta(hours=1)).order_by(Event.start_date).all()
        for event in events:
            buttons.append({'data': f'goto_event_{event.id}', 'name': _("Show event") + f" {event.get_translation(context.user_data['user'].language_code).name}"})
        return buttons


class AgroportInstantDistributionByEventMenu(AgroportInstantDistributionMenu):
    menu_name = 'distribution_by_event'

    class States(enum.Enum):
        ACTION = 1
        NEW_MESSAGE = 2
        VIEW_MESSAGE = 3
        EDIT_CAPTION = 4
        ADD_BUTTON = 5
        SELECT_EVENT = 6

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^distribution_by_event$', pass_user_data=True)]

    def entry(self, update, context):
        _ = context.user_data['_']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['messages'] = []
        context.user_data[self.menu_name]['selected_message'] = None
        context.user_data[self.menu_name]['buttons'] = []

        self.ask_event(context)
        return self.States.SELECT_EVENT

    def ask_event(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        flat_buttons = []
        events = DBSession.query(Event).join(Event.subevents).filter(Event.end_date > datetime.datetime.now() + datetime.timedelta(hours=1)).order_by(Event.start_date).all()
        for event in events:
            flat_buttons.append(InlineKeyboardButton(event.get_translation(context.user_data['user'].language_code).name, callback_data=f"set_event_{event.id}"))

        buttons = group_buttons(flat_buttons, (len(flat_buttons) // 10) + 1)
        buttons.extend([[InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")]])

        self.send_or_edit(context, chat_id=user.chat_id, text=_("Please select event:"), reply_markup=InlineKeyboardMarkup(buttons))

    def set_event(self, update, context):
        data = update.callback_query.data

        context.user_data[self.menu_name]['event_filter_id'] = int(data.replace("set_event_", ""))
        self.send_message(context)
        return self.States.ACTION

    def ask_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Messages will be sent to users who bought tickets to selected event. Continue?")

    def after_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Message was sent to all bot users who bought tickets to selected event except you")

    def users_to_send(self, context):
        tickets = DBSession.query(Ticket) \
            .join(Ticket.user) \
            .join(Ticket.event) \
            .filter(Ticket.event_id == context.user_data[self.menu_name]['event_filter_id']) \
            .filter(User.active == True) \
            .filter(User.chat_id != context.user_data["user"].chat_id) \
            .all()

        return [ticket.user for ticket in tickets]

    def additional_states(self):
        return {self.States.SELECT_EVENT: [CallbackQueryHandler(self.set_event, pattern='^set_event_\d+$'),
                                           CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                           MessageHandler(Filters.all, to_state(self.States.SELECT_EVENT))]}


class AgroportInstantDistributionBySubeventMenu(AgroportInstantDistributionMenu):
    menu_name = 'distribution_by_subevent'

    class States(enum.Enum):
        ACTION = 1
        NEW_MESSAGE = 2
        VIEW_MESSAGE = 3
        EDIT_CAPTION = 4
        ADD_BUTTON = 5
        SELECT_EVENT = 6
        SELECT_SUBEVENT = 7

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^distribution_by_subevent$', pass_user_data=True)]

    def entry(self, update, context):
        _ = context.user_data['_']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['messages'] = []
        context.user_data[self.menu_name]['selected_message'] = None
        context.user_data[self.menu_name]['buttons'] = []

        self.ask_event(context)
        return self.States.SELECT_EVENT

    def ask_event(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        flat_buttons = []
        events = DBSession.query(Event).join(Event.subevents).filter(Event.end_date > datetime.datetime.now() + datetime.timedelta(hours=1)).order_by(Event.start_date).all()
        for event in events:
            flat_buttons.append(InlineKeyboardButton(event.get_translation(context.user_data['user'].language_code).name, callback_data=f"set_event_{event.id}"))

        buttons = group_buttons(flat_buttons, (len(flat_buttons) // 10) + 1)
        buttons.extend([[InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")]])

        self.send_or_edit(context, chat_id=user.chat_id, text=_("Please select event:"), reply_markup=InlineKeyboardMarkup(buttons))

    def set_event(self, update, context):
        data = update.callback_query.data

        context.user_data[self.menu_name]['event_filter_id'] = int(data.replace("set_event_", ""))
        self.ask_subevent(context)
        return self.States.SELECT_SUBEVENT

    def ask_subevent(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        flat_buttons = []
        subevents = DBSession.query(Subevent).join(Subevent.event)\
            .filter(Event.end_date > datetime.datetime.now() + datetime.timedelta(hours=1))\
            .filter(Subevent.event_id == context.user_data[self.menu_name]['event_filter_id']).all()

        for subevent in subevents:
            flat_buttons.append(InlineKeyboardButton(subevent.get_translation(context.user_data['user'].language_code).name, callback_data=f"set_subevent_{subevent.id}"))

        buttons = group_buttons(flat_buttons, (len(flat_buttons) // 10) + 1)
        buttons.extend([[InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")]])

        self.send_or_edit(context, chat_id=user.chat_id, text=_("Please select subevent:"), reply_markup=InlineKeyboardMarkup(buttons))

    def set_subevent(self, update, context):
        data = update.callback_query.data

        context.user_data[self.menu_name]['subevent_filter_id'] = int(data.replace("set_subevent_", ""))
        self.send_message(context)
        return self.States.ACTION

    def ask_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Messages will be sent to users who bought tickets to selected subevent. Continue?")

    def after_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Message was sent to all bot users who bought tickets to selected subevent except you")

    def users_to_send(self, context):
        subevent = DBSession.query(Subevent).get(context.user_data[self.menu_name]['subevent_filter_id'])

        return [ticket.user for ticket in subevent.tickets]

    def additional_states(self):
        return {self.States.SELECT_EVENT: [CallbackQueryHandler(self.set_event, pattern='^set_event_\d+$'),
                                           CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                           MessageHandler(Filters.all, to_state(self.States.SELECT_EVENT))],
                self.States.SELECT_SUBEVENT: [CallbackQueryHandler(self.set_subevent, pattern='^set_subevent_\d+$'),
                                              CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                              MessageHandler(Filters.all, to_state(self.States.SELECT_SUBEVENT))]
                }


class AgroportInstantDistributionByCategoryMenu(AgroportInstantDistributionMenu):
    menu_name = 'distribution_by_category'

    class States(enum.Enum):
        ACTION = 1
        NEW_MESSAGE = 2
        VIEW_MESSAGE = 3
        EDIT_CAPTION = 4
        ADD_BUTTON = 5
        SELECT_CATEGORY = 6

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^distribution_by_category$', pass_user_data=True)]

    def entry(self, update, context):
        _ = context.user_data['_']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['messages'] = []
        context.user_data[self.menu_name]['selected_message'] = None
        context.user_data[self.menu_name]['buttons'] = []

        self.ask_category(context)
        return self.States.SELECT_CATEGORY

    def ask_category(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        flat_buttons = []
        categories = DBSession.query(Category).join(Category.events).filter(Event.end_date > datetime.datetime.now() + datetime.timedelta(hours=1)).all()
        for category in categories:
            flat_buttons.append(InlineKeyboardButton(category.get_translation(context.user_data['user'].language_code).name, callback_data=f"set_category_{category.id}"))

        buttons = group_buttons(flat_buttons, (len(flat_buttons) // 10) + 1)
        buttons.extend([[InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")]])

        self.send_or_edit(context, chat_id=user.chat_id, text=_("Please select event:"), reply_markup=InlineKeyboardMarkup(buttons))

    def set_category(self, update, context):
        data = update.callback_query.data

        context.user_data[self.menu_name]['category_filter_id'] = int(data.replace("set_category_", ""))
        self.send_message(context)
        return self.States.ACTION

    def ask_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Messages will be sent to users who bought tickets to event in selected category. Continue?")

    def after_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Message was sent to all bot users who bought tickets to event in selected category except you")

    def users_to_send(self, context):
        tickets = DBSession.query(Ticket) \
            .join(Ticket.user) \
            .join(Ticket.event) \
            .filter(Event.category_id == context.user_data[self.menu_name]['category_filter_id']) \
            .filter(User.active == True) \
            .filter(User.chat_id != context.user_data["user"].chat_id) \
            .all()

        return [ticket.user for ticket in tickets]

    def additional_states(self):
        return {self.States.SELECT_CATEGORY: [CallbackQueryHandler(self.set_category, pattern='^set_category_\d+$'),
                                           CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                           MessageHandler(Filters.all, to_state(self.States.SELECT_CATEGORY))]}


class AgroportInstantDistributionByRegionMenu(AgroportInstantDistributionMenu):
    menu_name = 'distribution_by_region'

    class States(enum.Enum):
        ACTION = 1
        NEW_MESSAGE = 2
        VIEW_MESSAGE = 3
        EDIT_CAPTION = 4
        ADD_BUTTON = 5
        SELECT_REGION = 6

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern='^distribution_by_region$', pass_user_data=True)]

    def entry(self, update, context):
        _ = context.user_data['_']

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        context.user_data[self.menu_name]['messages'] = []
        context.user_data[self.menu_name]['selected_message'] = None
        context.user_data[self.menu_name]['buttons'] = []

        self.ask_region(context)
        return self.States.SELECT_REGION

    def ask_region(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        flat_buttons = []
        settings = get_settings(SETTINGS_FILE)
        regions = settings['regions'].get(user.language_code, settings['regions']['en'])

        for key, value in regions.items():
            flat_buttons.append(InlineKeyboardButton(value, callback_data=f"set_region_{key}"))

        buttons = group_buttons(flat_buttons, (len(flat_buttons) // 10) + 1)
        buttons.extend([[InlineKeyboardButton(_("Back"), callback_data=f"back_{self.menu_name}")]])

        self.send_or_edit(context, chat_id=user.chat_id, text=_("Please select region:"), reply_markup=InlineKeyboardMarkup(buttons))

    def set_region(self, update, context):
        data = update.callback_query.data

        context.user_data[self.menu_name]['region_filter_name'] = data.replace("set_region_", "")
        self.send_message(context)
        return self.States.ACTION

    def ask_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Messages will be sent to users who live in selected region. Continue?")

    def after_start_distribution_text(self, context):
        _ = context.user_data['_']
        return _("Message was sent to all bot users who live in selected region except you")

    def users_to_send(self, context):
        return DBSession.query(User) \
            .filter(User.chat_id != context.user_data["user"].chat_id) \
            .filter(User.active == True) \
            .filter(User.region == context.user_data[self.menu_name]['region_filter_name']) \
            .all()

    def additional_states(self):
        return {self.States.SELECT_REGION: [CallbackQueryHandler(self.set_region, pattern='^set_region_.+$'),
                                            CallbackQueryHandler(self.back, pattern=f"^back_{self.menu_name}$"),
                                            MessageHandler(Filters.all, to_state(self.States.SELECT_REGION))]}
