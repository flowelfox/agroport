import csv
import datetime
import enum
from io import StringIO

import requests
from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import to_state, unknown_command
from formencode import Invalid, validators
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.models import DBSession, Event, Subevent, SubeventTranslation


class SubeventsCSVLoaderMenu(BaseMenu):
    menu_name = 'subevent_csv_loader_menu'

    class States(enum.Enum):
        CSV_WAIT = 1
        END = 2

    def entry(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('upload_subevents_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}

        self.send_message(context)
        return self.States.CSV_WAIT

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data["_"]
        message_text = _("Please send me CSV file.")
        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back_from_csv')]]

        self.send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    def back(self, update, context):
        self.parent.update_objects(context)
        self.parent.send_message(context)
        return ConversationHandler.END

    def load_csv_file(self, update, context):
        user = context.user_data['user']
        _ = context.user_data["_"]
        file = StringIO(update.message.document.get_file().download_as_bytearray().decode("utf-8"))
        dr = csv.DictReader(file)
        data = []
        errors = []
        image_validator = validators.URL()
        for idx, row in enumerate(dr):
            has_errors = False
            row_data = {'name': {},
                        'description': {},
                        'place': {},
                        'image': {},
                        'organizers': {},
                        }
            for key in row:
                if key == 'ID' or key == 'id':
                    try:
                        row_data['id'] = int(row[key])
                    except ValueError:
                        errors.append({'row': idx + 1, 'column': key, 'message': "Value must be integer number"})
                        has_errors = True
                elif key.lower().startswith('event id'):
                    try:
                        event_id = int(row[key])
                        event = DBSession.query(Event).get(event_id)
                        if event is None:
                            errors.append({'row': idx + 1, 'column': key, 'message': f"Event with ID {event_id} not found"})
                        else:
                            row_data['event_id'] = event_id
                    except ValueError:
                        errors.append({'row': idx + 1, 'column': key, 'message': "Value must be integer number"})
                        has_errors = True
                elif key.lower().startswith("date"):
                    try:
                        row_data['date'] = datetime.datetime.strptime(row[key], '%H:%M %d.%m.%Y')
                    except ValueError:
                        errors.append({'row': idx + 1, 'column': key, 'message': "Invalid date format"})
                        has_errors = True
                elif key.lower().startswith("ticket limit"):
                    try:
                        row_data['ticket_limit'] = int(row[key])
                    except ValueError:
                        errors.append({'row': idx + 1, 'column': key, 'message': "Value must be integer number"})
                        has_errors = True

                elif key.lower().startswith("price"):
                    try:
                        row_data['price'] = int(row[key])
                    except ValueError:
                        errors.append({'row': idx + 1, 'column': key, 'message': "Value must be integer number"})
                        has_errors = True

                elif key.lower().startswith("name"):
                    if key.endswith('EN'):
                        row_data['name']['en'] = row[key]
                    elif key.endswith('RU'):
                        row_data['name']['ru'] = row[key]
                    elif key.endswith('UK'):
                        row_data['name']['uk'] = row[key]
                elif key.lower().startswith("description"):
                    if key.endswith('EN'):
                        row_data['description']['en'] = row[key]
                    elif key.endswith('RU'):
                        row_data['description']['ru'] = row[key]
                    elif key.endswith('UK'):
                        row_data['description']['uk'] = row[key]
                elif key.lower().startswith("place"):
                    if key.endswith('EN'):
                        row_data['place']['en'] = row[key]
                    elif key.endswith('RU'):
                        row_data['place']['ru'] = row[key]
                    elif key.endswith('UK'):
                        row_data['place']['uk'] = row[key]
                elif key.lower().startswith("image"):
                    if key.endswith('EN'):
                        row_data['image']['en'] = row[key]
                    elif key.endswith('RU'):
                        row_data['image']['ru'] = row[key]
                    elif key.endswith('UK'):
                        row_data['image']['uk'] = row[key]
                elif key.lower().startswith("organizers"):
                    if key.endswith('EN'):
                        row_data['organizers']['en'] = row[key]
                    elif key.endswith('RU'):
                        row_data['organizers']['ru'] = row[key]
                    elif key.endswith('UK'):
                        row_data['organizers']['uk'] = row[key]

            if not has_errors:
                if 'image' in row_data:
                    for link in row_data['image'].values():
                        try:
                            image_validator.to_python(link)
                            res = requests.get(link)
                            if res.status_code != 200:
                                errors.append({'row': idx + 1, 'column': 'image', 'message': "Wrong image link"})
                                has_errors = True
                        except Invalid:
                            errors.append({'row': idx + 1, 'column': 'image', 'message': "Wrong image link"})
                            has_errors = True

                if 'id' not in row_data:
                    errors.append({'row': idx + 1, 'column': None, 'message': "ID value is required"})
                    has_errors = True
                if 'event_id' not in row_data:
                    errors.append({'row': idx + 1, 'column': None, 'message': "Event ID value is required"})
                    has_errors = True
                if 'date' not in row_data:
                    errors.append({'row': idx + 1, 'column': None, 'message': "Date value is required"})
                    has_errors = True
                if 'ticket_limit' not in row_data:
                    errors.append({'row': idx + 1, 'column': None, 'message': "Ticket limit value is required"})
                    has_errors = True
                if 'en' not in row_data['name']:
                    errors.append({'row': idx + 1, 'column': None, 'message': "Name EN translation is required"})
                    has_errors = True
                if 'en' not in row_data['description']:
                    errors.append({'row': idx + 1, 'column': None, 'message': "Description EN translation is required"})
                    has_errors = True
                if 'en' not in row_data['place']:
                    errors.append({'row': idx + 1, 'column': None, 'message': "Place EN translation is required"})
                    has_errors = True
                if 'en' not in row_data['organizers']:
                    errors.append({'row': idx + 1, 'column': None, 'message': "Organizers EN translation is required"})
                    has_errors = True
                if not all([row_data[col].get('ru', False) for col in ['name', 'description', 'place', 'organizers']]) and any([row_data[col].get('ru', False) for col in ['name', 'description', 'place', 'organizers']]):
                    errors.append({'row': idx + 1, 'column': None, 'message': "Translation must be present for all columns, not only some of them."})
                    has_errors = True

            if not has_errors:
                data.append(row_data)
        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back_from_csv')]]
        self.delete_interface(context)
        if errors:
            message_text = str(len(errors)) + ' ' + _("errors occurred while importing file:") + '\n'
            for idx, error in enumerate(errors):
                message_text += f'{idx + 1}. {_("Row")} №{error["row"]}, '
                message_text += f'{_("Column")} {error["column"]}, ' if error['column'] else ""
                message_text += error['message'] + '\n'

            message_text += '\n' + _("Please send me CSV file.")

            self.send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        else:
            for row_data in data:
                obj = DBSession.query(Subevent).get(row_data['id'])
                if obj is None:
                    obj = Subevent()

                obj.id = row_data['id']
                obj.event_id = row_data['event_id']
                obj.date = row_data['date']
                obj.ticket_limit = row_data['ticket_limit']
                obj.price = row_data['price']

                for lang in ['en', 'ru', 'uk']:
                    if row_data['name'].get(lang, False):
                        translation = DBSession.query(SubeventTranslation).filter(SubeventTranslation.subevent_id == obj.id).filter(SubeventTranslation.lang == lang.lower()).first()
                        if translation is None:
                            translation = SubeventTranslation(lang=lang)
                        translation.name = row_data['name'].get(lang, None)
                        translation.description = row_data['description'].get(lang, None)
                        translation.place = row_data['place'].get(lang, None)
                        translation.organizers = row_data['organizers'].get(lang, None)
                        image_link = row_data['image'].get(lang, None)
                        if image_link:
                            mes = self.bot.send_photo(chat_id=user.chat_id, photo=image_link)
                            translation.image = mes.photo[-1].file_id
                            mes.delete()
                        DBSession.add(translation)
                        obj.translations.append(translation)

                DBSession.add(obj)
            DBSession.commit()

            self.send_or_edit(context, chat_id=user.chat_id, text=_("Data imported successfully"), reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.CSV_WAIT

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^upload_subevents_csv$')],
                                      states={self.States.CSV_WAIT: [CallbackQueryHandler(self.back, pattern="^back_from_csv$"),
                                                                     MessageHandler(Filters.document.mime_type('text/csv'), self.load_csv_file),
                                                                     MessageHandler(Filters.all, to_state(self.States.CSV_WAIT))],
                                              self.States.END: [CallbackQueryHandler(self.back, pattern="^back_from_csv$"),
                                                                MessageHandler(Filters.all, to_state(self.States.END))],
                                              },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
