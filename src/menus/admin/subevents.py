from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, group_buttons
from botmanlib.menus.validators import DateTimeConverter
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, ConversationHandler

from src.menus.admin.subevents_csv_loader import SubeventsCSVLoaderMenu
from src.models import DBSession, Subevent, SubeventTranslation


class SubeventsMenu(OneListMenu):
    menu_name = 'subevents_menu'

    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('subevents_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        return DBSession.query(Subevent).filter(Subevent.event_id == self.parent.selected_object(context).id).order_by(Subevent.date).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^admin_subevents$")]

    def message_text(self, context, obj):
        _ = context.user_data['_']
        if obj:
            message_text = f"____{_('Subevents')}____\n"
            en_translation = obj.get_translation('en')
            message_text += _("Subevent ID") + f": {obj.id}\n"
            message_text += (_("Name") + f" {en_translation.lang.upper()}: {en_translation.name}" + '\n') if en_translation else ""
            message_text += (_("Description") + f" {en_translation.lang.upper()}: {en_translation.description}" + '\n') if en_translation else ""
            message_text += _("Date") + f": {obj.date.strftime('%H:%M %d.%m.%Y')}\n"
            message_text += (_("Place") + f" {en_translation.lang.upper()}: {en_translation.place}" + '\n') if en_translation else ""
            message_text += (_("Image") + f" {en_translation.lang.upper()}: {_('Loaded') if en_translation.image else _('Not loaded')}" + '\n') if en_translation else ""
            message_text += _("Ticket limit") + f": {obj.ticket_limit}\n"
            event_en = obj.event.get_translation('en')
            message_text += _("Event") + f": {event_en.name if event_en else _('Not set')}\n"
            message_text += (_("Organizers") + f" {en_translation.lang.upper()}: {en_translation.organizers}" + '\n') if en_translation else ""
            available_translation = ','.join([trans.lang.upper() for trans in obj.translations])
            message_text += _("Price") + f": {obj.price if obj.price is not None else _('Unknown')}\n"
            message_text += _("Languages") + f": {available_translation if available_translation else 'Empty'}\n"

            if en_translation is None:
                message_text += '  ' + _("There is not translations yet") + '\n'
        else:
            message_text = _("There is not subevents yet") + '\n'

        return message_text

    def delete_ask(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('allow_delete_subevent'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this action"), show_alert=True)
            return self.States.ACTION

        return super(SubeventsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        if user.has_permission('add_subevent_menu_access'):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_subevent"))
        if user.has_permission('allow_delete_subevent') and obj:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))

        if obj:
            if user.has_permission('edit_subevent_menu_access'):
                buttons.append(InlineKeyboardButton(_("Edit"), callback_data="edit_subevent"))
        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        if obj:
            if user.has_permission('subevent_translations_menu_access'):
                buttons.append(InlineKeyboardButton(_("Translations"), callback_data='subevent_translations'))
        if user.has_permission('upload_subevents_menu_access'):
            buttons.append(InlineKeyboardButton(_("Upload from CSV"), callback_data='upload_subevents_csv'))
        return group_buttons(buttons, 1)

    def additional_states(self):
        subevent_add_menu = SubeventAddMenu(self)
        subevent_edit_menu = SubeventEditMenu(self)
        subevent_translations = SubeventTranslationsMenu(self)
        subevents_csv_loader_menu = SubeventsCSVLoaderMenu(self)

        return {self.States.ACTION: [subevent_add_menu.handler,
                                     subevent_edit_menu.handler,
                                     subevent_translations.handler,
                                     subevents_csv_loader_menu.handler
                                     ]}

    def after_delete_text(self, context):
        _ = context.user_data['_']
        return _("Event deleted")

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "_______" + _("Subevent") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + "_______"


class SubeventAddMenu(ArrowAddEditMenu):
    menu_name = "subevent_add_menu"
    model = Subevent

    def entry(self, update, context):
        self._entry(update, context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('add_subevent_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        return None

    def fields(self, context):
        _ = context.user_data['_']
        fields = [self.Field('name', _("*Name EN"), validators.String(), required=True),
                  self.Field('description', _("*Description EN"), validators.String(), required=True),
                  self.Field('date', _("*Start date"), DateTimeConverter(), required=True),
                  self.Field('ticket_limit', _("*Ticket limit"), validators.Int(min=0), required=True),
                  self.Field('place', _("*Place EN"), validators.String(), required=True),
                  self.Field('image', _("*Image EN"), validators.String(), is_photo=True, required=True),
                  self.Field('organizers', _("*Organizers EN"), validators.String(), required=True),
                  self.Field('price', _("*Price"), validators.Number(min=0), default=0, required=True),
                  ]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_subevent$")]

    def save_object(self, obj, context, session=None):
        user_data = context.user_data
        translation = SubeventTranslation(lang='en')
        translation.name = user_data[self.menu_name]['name']
        translation.description = user_data[self.menu_name]['description']
        translation.place = user_data[self.menu_name]['place']
        translation.image = user_data[self.menu_name]['image']
        translation.organizers = user_data[self.menu_name]['organizers']
        obj.translations.append(translation)
        obj.event = self.parent.parent.selected_object(context)

        if not add_to_db([translation, obj], session):
            return self.conv_fallback(context)


class SubeventEditMenu(ArrowAddEditMenu):
    menu_name = "subevent_edit_menu"
    model = Subevent

    def entry(self, update, context):
        self._entry(update, context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('edit_subevent_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        subevent = self.parent.selected_object(context)
        if subevent:
            return DBSession.query(Subevent).filter(Subevent.id == subevent.id).first()
        else:
            self.parent.update_objects(context)
            self.parent.send_message(context)
            return ConversationHandler.END

    def fields(self, context):
        _ = context.user_data['_']
        fields = [self.Field('date', _("*Date"), DateTimeConverter(), required=True),
                  self.Field('ticket_limit', _("*Ticket limit"), validators.Int(min=0), required=True),
                  self.Field('price', _("*Price"), validators.Number(min=0), default=0, required=True),
                  ]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^edit_subevent$")]


class SubeventTranslationsMenu(OneListMenu):
    menu_name = 'subevent_translations_menu'

    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('subevent_translations_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        subevent = self.parent.selected_object(context)
        if subevent:
            return DBSession.query(SubeventTranslation).filter(SubeventTranslation.subevent_id == subevent.id).all()
        else:
            return []

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^subevent_translations$", pass_user_data=True)]

    def message_text(self, context, obj):
        _ = context.user_data['_']
        if obj:
            message_text = _("Subevent") + f" {obj.subevent_id} {_('translations')}:\n"
            message_text += _("Language") + f": {obj.lang.upper()}\n"
            message_text += _("Name") + f": {obj.name}\n"
            message_text += _("Description") + f": {obj.description}\n"
            message_text += _("Place") + f": {obj.place}\n"
            message_text += _("Image") + f": {_('Loaded') if obj.image else _('Not loaded')}" + '\n'
            message_text += _("Organizers") + f": {obj.organizers}\n"
        else:
            message_text = _("There is not translations yet") + '\n'

        return message_text

    def delete_ask(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('allow_delete_subevent_translation'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this action"), show_alert=True)
            return self.States.ACTION

        return super(SubeventTranslationsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        exist_translations = [trans[0].upper() for trans in DBSession.query(SubeventTranslation.lang).filter(SubeventTranslation.subevent == self.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)

        if available_translations and user.has_permission('add_subevent_translation_menu_access'):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_subevent_trans"))
        if user.has_permission('allow_delete_subevent_translation') and len(exist_translations) > 1:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        if obj and user.has_permission('edit_subevent_translation_menu_access'):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"edit_subevent_trans"))
        return buttons

    def additional_states(self):
        category_trans_add_edit_menu = SubeventTranslationAddEditMenu(self)
        return {self.States.ACTION: [category_trans_add_edit_menu.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['_']
        return _("Translation deleted")


class SubeventTranslationAddEditMenu(ArrowAddEditMenu):
    menu_name = "subevent_translation_add_edit_menu"
    model = SubeventTranslation
    auto_switch_to_next_field_after_last_field = False

    def entry(self, update, context):
        self._entry(update, context)
        user = context.user_data['user']
        _ = context.user_data['_']
        self.load(context)
        if self.action(context) is self.Action.ADD and not user.has_permission('add_subevent_translation_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END
        if self.action(context) is self.Action.EDIT and not user.has_permission('edit_subevent_translation_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        return self.States.ACTION

    def variant_center_buttons(self, context):
        return []

    def query_object(self, context):
        cat_trans = self.parent.selected_object(context)
        if cat_trans and self.action(context) == self.Action.EDIT:
            return cat_trans
        else:
            return None

    def fields(self, context):
        _ = context.user_data['_']

        exist_translations = [trans[0].upper() for trans in DBSession.query(SubeventTranslation.lang).filter(SubeventTranslation.subevent == self.parent.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if self.action(context) == self.Action.ADD:
            fields = [self.Field('lang', _("*Language"), validators.String(), variants=available_translations, required=True)]
        else:
            fields = []
        fields += [self.Field('name', _("*Name"), validators.String(), required=True),
                   self.Field('description', _("*Description"), validators.String(), required=True),
                   self.Field('place', _("*Place"), validators.String(), required=True),
                   self.Field('image', _("*Image"), validators.String(), is_photo=True, required=True),
                   self.Field('organizers', _("*Organizers"), validators.String(), required=True)]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_subevent_trans$", pass_user_data=True),
                CallbackQueryHandler(self.entry, pattern="^edit_subevent_trans$", pass_user_data=True)]

    def save_object(self, obj, context, session=None):
        obj.lang = obj.lang.lower()
        obj.subevent = self.parent.parent.selected_object(context)

        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def message_top_text(self, context):
        _ = context.user_data['_']
        if self.action(context) == self.Action.EDIT:
            text = "_____" + _("Edit subevent translation") + "_____\n"
            text += '       ' + _("Language") + f": {self.query_object(context).lang.upper()}\n"
        else:
            text = "___________\n"
        return text
