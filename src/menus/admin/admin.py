import enum

from botmanlib.menus.basemenu import BaseMenu
from botmanlib.menus.helpers import generate_underscore, add_to_db, unknown_command, to_state, group_buttons
from botmanlib.menus.redy_to_use.permissions import PermissionsMenu
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import ConversationHandler, Filters, MessageHandler, CallbackQueryHandler

from src.menus.admin.categories import CategoriesMenu
from src.menus.admin.distributions import DistributionsMenu
from src.menus.admin.events import EventsMenu
from src.menus.admin.qr_scanner import QRScannerMenu
from src.menus.admin.ticket_returns import TicketReturnsMenu
from src.menus.admin.users_and_tickets import UsersInfoMenu, TicketsInfoMenu
from src.models import DBSession, User, Permission


class AdminMenu(BaseMenu):
    menu_name = 'admin_menu'

    class States(enum.Enum):
        ACTION = 1

    def entry(self, update, context):
        user = DBSession.query(User).filter(User.chat_id == update.effective_user.id).first()
        tuser = update.effective_user
        if not user:
            user = User()
        user.chat_id = tuser.id
        user.first_name = tuser.first_name
        user.last_name = tuser.last_name
        user.username = tuser.username
        user.active = True

        if not add_to_db(user):
            return self.conv_fallback(context)

        user_data = context.user_data
        user_data['user'] = user
        _ = user_data['_'] = generate_underscore(user.language_code)

        if not user.has_permission('admin_menu_access'):
            return ConversationHandler.END

        if self.menu_name not in user_data:
            user_data[self.menu_name] = {}

        if 'main' in user_data:
            self.delete_interface(context, interface_name='main')
        else:
            self.send_or_edit(context, interface_name='main', chat_id=user.chat_id, text=_("Select an action:"), reply_markup=ReplyKeyboardRemove())
            self.delete_interface(context, interface_name='main')
        self.send_message(context)

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)

        return self.States.ACTION

    def send_message(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = []
        if user.has_permission('admin_events_menu_access'):
            buttons.append(InlineKeyboardButton(_("Events"), callback_data='admin_events'))
        if user.has_permission('permissions_menu_access'):
            buttons.append(InlineKeyboardButton(_("Permissions"), callback_data='permissions'))
        if user.has_permission('distribution_menu_access'):
            buttons.append(InlineKeyboardButton(_("Distribution"), callback_data='distribution'))
        if user.has_permission('categories_menu_access'):
            buttons.append(InlineKeyboardButton(_("Categories"), callback_data='categories'))
        if user.has_permission('qr_scanner_menu_access'):
            buttons.append(InlineKeyboardButton(_("QR Scanner"), callback_data='qr_scanner'))
        if user.has_permission('ticket_returns_menu_access'):
            buttons.append(InlineKeyboardButton(_("Ticket return requests"), callback_data='ticket_returns'))
        if user.has_permission('users_info_menu_access'):
            buttons.append(InlineKeyboardButton(_("Users info"), callback_data='users_info'))
        if user.has_permission('user_tickets_menu_access'):
            buttons.append(InlineKeyboardButton(_("Tickets info"), callback_data='tickets_info'))

        buttons.append(InlineKeyboardButton(_("Back to main menu"), callback_data='goto_start'))

        self.send_or_edit(context, chat_id=user.chat_id, text=_("Admin menu:"), reply_markup=InlineKeyboardMarkup(group_buttons(buttons, 1)))

    def goto_start(self, update, context):
        self.delete_interface(context)
        update.callback_query.data = 'start'
        context.update_queue.put(update)
        return ConversationHandler.END

    def get_handler(self):
        permissions_menu = PermissionsMenu(User, Permission, parent=self)
        categories_menu = CategoriesMenu(self)
        events_menu = EventsMenu(self)
        ticket_returns = TicketReturnsMenu(self)
        qr_scanner_menu = QRScannerMenu(self)
        self.users_info_menu = UsersInfoMenu(self)
        self.tickets_info_menu = TicketsInfoMenu(self)

        distribution_menu = DistributionsMenu(self)

        handler = ConversationHandler(entry_points=[MessageHandler(Filters.regex('^/admin$'), self.entry)],
                                      states={
                                          AdminMenu.States.ACTION: [distribution_menu.handler,
                                                                    CallbackQueryHandler(self.entry, pattern='^back$'),
                                                                    CallbackQueryHandler(self.goto_start, pattern='^goto_start$'),
                                                                    permissions_menu.handler,
                                                                    categories_menu.handler,
                                                                    events_menu.handler,
                                                                    ticket_returns.handler,
                                                                    qr_scanner_menu.handler,
                                                                    self.users_info_menu.handler,
                                                                    self.tickets_info_menu.handler,
                                                                    MessageHandler(Filters.regex('^/start$'), self.goto_start),
                                                                    MessageHandler(Filters.all, to_state(AdminMenu.States.ACTION))],

                                      },
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
