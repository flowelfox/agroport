from io import BytesIO

from botmanlib.menus import BaseMenu
from botmanlib.menus.helpers import to_state, unknown_command, get_settings
from pyzbar.pyzbar import decode
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters
from PIL import Image
from emoji import emojize

from src.models import DBSession, Ticket
from src.settings import SETTINGS_FILE


class QRScannerMenu(BaseMenu):
    menu_name = 'qr_scanner_menu'

    def entry(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('qr_scanner_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        if self.menu_name not in context.user_data:
            context.user_data[self.menu_name] = {}
        self.send_message(context)
        return self.States.ACTION

    def send_message(self, context):
        _ = context.user_data['_']
        message_text = _("Please send photo of qr code")
        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back')]]

        self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))

    def process_qr(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = [[InlineKeyboardButton(_("Back"), callback_data='back')]]
        self.delete_interface(context)
        with BytesIO() as file:
            photo = update.effective_message.photo[-1]
            photo.get_file().download(out=file)
            file.seek(0)
            decoded_data = decode(Image.open(file))
            if decoded_data:
                message_text = ""
                raw_data_message = emojize(":gear: <b>") + _("Service data:") + '</b>\n'
                raw_data_message += emojize(":wavy_dash:") * 10
                raw_data_message += '\n'
                raw_data_message += '<b>' + _("String with data") + f":</b> {decoded_data[0].data.decode('utf-8')}\n"
                try:
                    ticket_id, user_id, event_id, subevents_ids = decoded_data[0].data.decode('utf-8').split("|")
                    subevents_ids = subevents_ids.split('-')
                    ticket_id = int(ticket_id)
                    user_id = int(user_id)
                    event_id = int(event_id)
                    raw_data_message += '<b>' + _("Ticket ID") + f":</b> {ticket_id}\n"
                    raw_data_message += '<b>' + _("User ID") + f":</b> {user_id}\n"
                    raw_data_message += '<b>' + _("Event ID") + f":</b> {event_id}\n"
                    raw_data_message += '<b>' + _("Subevents ID's") + f":</b> {','.join(subevents_ids)}\n\n"
                    ticket = DBSession.query(Ticket).get(ticket_id)
                except ValueError:
                    self.logger.error("Wrong qr code.")
                    message_text += _("Qr code does not contain ticket info") + '\n'
                    ticket = None

                if ticket:
                    message_text += emojize(":white_heavy_check_mark: ") + _("Ticket found.") + '\n\n'

                    message_text += emojize(":information: <b>") + _("Ticket information:") + '</b>\n'
                    message_text += emojize(":wavy_dash:") * 10
                    message_text += '\n'
                    message_text += '<b>' + _("User name") + f":</b> {ticket.user.desired_name if ticket.user.desired_name else _('Unknown')}\n"
                    message_text += '<b>' + _("Company") + f":</b> {ticket.user.company if ticket.user.company else _('Unknown')}\n"
                    message_text += '<b>' + _("Event") + f":</b> {ticket.event.get_translation(user.language_code).name}\n"
                    message_text += '<b>' + _("Bought") + f":</b> {ticket.create_date.strftime('%H:%M %d.%m.%Y')}\n\n"
                    for subevent in ticket.subevents:
                        subevent_translation = subevent.get_translation(user.language_code)
                        message_text += emojize(":small_blue_diamond: <b>") + subevent_translation.name + "</b>\n"
                        message_text += emojize(":wavy_dash:") * 10
                        message_text += '\n'
                        message_text += emojize(":round_pushpin: ") + subevent_translation.place + "\n"
                        message_text += emojize(":shopping_cart: ") + (f"{subevent.price:.0f} {_('UAH')}" if subevent.price else _('Free')) + "\n\n"
                    message_text += '\n'

                    if user.has_permission('allow_view_full_qr_info') or user.has_permission('allow_view_additional_qr_info'):
                        settings = get_settings(SETTINGS_FILE)
                        regions = settings['regions'].get(user.language_code, settings['regions']['en'])
                        regions['non_resident'] = emojize(':globe_with_meridians: ') + _("Non-resident")
                        occupations = {
                            'farmer': emojize(":sunflower: ") + _("Farmer"),
                            'products_services_agro': emojize(":tractor: ") + _("Product/Services for Agro"),
                            'other': emojize(":speech_balloon: ") + _("Other Activities")
                        }

                        message_text += "<b>" + _("User information:") + '</b>\n'
                        message_text += emojize(":wavy_dash:") * 10
                        message_text += '\n'

                        message_text += '<b>' + _("Name") + f":</b> {ticket.user.get_name()}\n"
                        message_text += '<b>' + _("Username") + f":</b> {'@' + ticket.user.username if ticket.user.username else _('Unknown')}\n"
                        message_text += '<b>' + _("Email") + f":</b> {ticket.user.email if ticket.user.email else _('Unknown')}\n"
                        message_text += '<b>' + _("Phone") + f":</b> {ticket.user.phone if ticket.user.phone else _('Unknown')}\n"
                        message_text += '<b>' + _("Region") + f":</b> {regions.get(ticket.user.region, _('Unknown')) if ticket.user.region else _('Unknown')}\n"
                        message_text += '<b>' + _("Occupation") + f":</b> {occupations.get(ticket.user.occupation, _('Unknown')) if ticket.user.occupation else _('Unknown')}\n"
                        message_text += '<b>' + _("First bot start") + f":</b> {ticket.user.join_date.strftime('%H:%M %d.%m.%Y')}\n"
                        #
                        # if ticket.return_request:
                        #     message_text += '  ' + _("Return request created") + f": {ticket.return_request.create_date.strftime('%H:%M %d.%m.%Y')}\n"
                        #     message_text += '  ' + _("Return request processed") + f": {ticket.return_request.processed_date.strftime('%H:%M %d.%m.%Y') if ticket.return_request.processed_date else _('Not processed yet')}\n"
                        #     message_text += '  ' + _("Return request reason") + f": {ticket.return_request.reason if ticket.return_request.reason else _('Unknown')}\n"
                        #     message_text += '  ' + _("Return request card") + f": {ticket.return_request.card if ticket.return_request.card else _('Unknown')}\n"
                        message_text += '\n'

                    if user.has_permission('allow_view_full_qr_info'):
                        message_text += '\n'
                        message_text += raw_data_message
                        message_text += "<i>" + _("You have \"View full QR info\" privilege.") + "</i>\n\n"
                    elif user.has_permission('allow_view_additional_qr_info') and not user.has_permission('allow_view_full_qr_info'):
                        message_text += "<i>" + _("You have \"View additional QR info\" privilege.") + "</i>\n\n"

                    if ticket.returned:
                        message_text += f"\n{emojize(':exclamation_mark:') * 5}{_('WARNING')}{emojize(':exclamation_mark:') * 5}\n"
                        message_text += '<i>' + _("This ticket was returned!") + "</i>"
                        message_text += f"\n{emojize(':exclamation_mark:') * 5}{_('WARNING')}{emojize(':exclamation_mark:') * 5}\n"
                    elif ticket.return_request:
                        message_text += f"\n{emojize(':exclamation_mark:') * 5}{_('WARNING')}{emojize(':exclamation_mark:') * 5}\n"
                        message_text += '<i>' + _("There is not processed return request for this ticket!") + "</i>"
                        message_text += f"\n{emojize(':exclamation_mark:') * 5}{_('WARNING')}{emojize(':exclamation_mark:') * 5}\n"

                    message_text += emojize(":wavy_dash:") * 10
                    message_text += '\n'
                    message_text += "\n" + _("Send next QR code.")
                else:
                    message_text += "\n" + _("QR code recognized but ticket not found.")
                self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")
            else:
                _ = context.user_data['_']
                message_text = _("Can't recognize qr code.")
                message_text += _("Please send photo of qr code")
                self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode="HTML")

        return self.States.ACTION

    def back(self, update, context):
        self.parent.send_message(context)
        return ConversationHandler.END

    def get_handler(self):
        handler = ConversationHandler(entry_points=[CallbackQueryHandler(self.entry, pattern='^qr_scanner$')],
                                      states={self.States.ACTION: [CallbackQueryHandler(self.back, pattern="^back$"),
                                                                   MessageHandler(Filters.photo, self.process_qr),
                                                                   MessageHandler(Filters.all, to_state(self.States.ACTION))]},
                                      fallbacks=[MessageHandler(Filters.all, unknown_command(-1))],
                                      allow_reentry=True)

        return handler
