import datetime

from botmanlib.menus import OneListMenu, ArrowAddEditMenu, BunchListMenu
from botmanlib.menus.helpers import add_to_db, group_buttons
from botmanlib.menus.validators import DateTimeConverter
from emoji import emojize
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, ConversationHandler

from src.menus.admin.events_csv_loader import EventsCSVLoaderMenu
from src.menus.admin.subevents import SubeventsMenu
from src.models import DBSession, Event, EventTranslation, Category


class EventsMenu(OneListMenu):
    menu_name = 'events_menu'

    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('admin_events_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        return DBSession.query(Event).filter(Event.end_date > datetime.datetime.now()).order_by(Event.start_date).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^admin_events$")]

    def message_text(self, context, obj):
        _ = context.user_data['_']
        if obj:
            message_text = f"____{_('Events')}____\n"
            en_translation = obj.get_translation('en')
            message_text += _("Event ID") + f": {obj.id}\n"
            message_text += (_("Name") + f" {en_translation.lang.upper()}: {en_translation.name}" + '\n') if en_translation else ""
            message_text += (_("Description") + f" {en_translation.lang.upper()}: {en_translation.description}" + '\n') if en_translation else ""
            message_text += _("Start date") + f": {obj.start_date.strftime('%H:%M %d.%m.%Y')}\n"
            message_text += _("End date") + f": {obj.end_date.strftime('%H:%M %d.%m.%Y')}\n"
            message_text += (_("Place") + f" {en_translation.lang.upper()}: {en_translation.place}" + '\n') if en_translation else ""
            message_text += (_("Image") + f" {en_translation.lang.upper()}: {_('Loaded') if en_translation.image else _('Not loaded')}" + '\n') if en_translation else ""
            category_en = obj.category.get_translation('en')
            message_text += _("Category") + f": {category_en.name if category_en else _('Not set')}\n"
            message_text += (_("Organizers") + f" {en_translation.lang.upper()}: {en_translation.organizers}" + '\n') if en_translation else ""
            available_translation = ','.join([trans.lang.upper() for trans in obj.translations])
            message_text += _("Languages") + f": {available_translation if available_translation else 'Empty'}\n"

            if en_translation is None:
                message_text += '  ' + _("There is not translations yet") + '\n'
        else:
            message_text = _("There is not events yet") + '\n'

        return message_text

    def delete_ask(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('allow_delete_event'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this action"), show_alert=True)
            return self.States.ACTION

        return super(EventsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []

        if user.has_permission('add_event_menu_access'):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_event"))
        if user.has_permission('allow_delete_event') and obj:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))

        if obj:
            if user.has_permission('edit_event_menu_access'):
                buttons.append(InlineKeyboardButton(_("Edit"), callback_data="edit_event"))
        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        if obj:
            if user.has_permission('subevents_menu_access'):
                buttons.append(InlineKeyboardButton(_("Subevents"), callback_data='admin_subevents'))
            if user.has_permission('event_translations_menu_access'):
                buttons.append(InlineKeyboardButton(_("Translations"), callback_data='event_translations'))

        if user.has_permission('upload_events_menu_access'):
            buttons.append(InlineKeyboardButton(_("Upload from CSV"), callback_data='upload_events_csv'))
        return group_buttons(buttons, 1)

    def additional_states(self):
        event_add_menu = EventAddMenu(self)
        event_edit_menu = EventEditMenu(self)
        event_translations = EventTranslationsMenu(self)
        events_csv_loader_menu = EventsCSVLoaderMenu(self)
        subevents_menu = SubeventsMenu(self)

        return {self.States.ACTION: [event_add_menu.handler,
                                     event_edit_menu.handler,
                                     event_translations.handler,
                                     events_csv_loader_menu.handler,
                                     subevents_menu.handler,
                                     ]}

    def after_delete_text(self, context):
        _ = context.user_data['_']
        return _("Event deleted")

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "_______" + _("Event") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + "_______"


class EventAddMenu(ArrowAddEditMenu):
    menu_name = "event_add_menu"
    model = Event

    def entry(self, update, context):
        self._entry(update, context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('add_event_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        return None

    def fields(self, context):
        _ = context.user_data['_']
        fields = [self.Field('name', _("*Name EN"), validators.String(), required=True),
                  self.Field('description', _("*Description EN"), validators.String(), required=True),
                  self.Field('start_date', _("*Start date"), DateTimeConverter(), required=True),
                  self.Field('end_date', _("*End date"), DateTimeConverter(), required=True),
                  self.Field('place', _("*Place EN"), validators.String(), required=True),
                  self.Field('image', _("*Image EN"), validators.String(), is_photo=True, required=True),
                  self.Field('organizers', _("*Organizers EN"), validators.String(), required=True),
                  self.Field('category_id', _("*Category EN"), validators.String(), required=True),

                  ]
        return fields

    def message_text(self, context):
        user_data = context.user_data
        _ = user_data['_']

        message_text = self.message_top_text(context)
        available_fields = self._available_fields(context)
        active_field = user_data[self.menu_name]['active_field']

        for idx, field in enumerate(available_fields):
            message_text += (emojize(':play_button:') if active_field == idx else '       ')
            value = user_data[self.menu_name][field.param]
            if isinstance(value, str):
                value = value.replace("\_", "_").replace("\*", "*").replace("\[", "[").replace("\]", "]")
            if isinstance(value, datetime.datetime):
                value = value.strftime("%H:%M %d.%m.%Y")
            if value is None:
                value = _("Unknown")
            elif field.param == 'category_id':
                value = DBSession.query(Category).get(value).get_translation(user_data['user'].language_code).name
            elif field.is_document or field.is_photo:
                value = _("Uploaded")
            else:
                if isinstance(value, list):
                    value = ', '.join(value)
                elif isinstance(value, bool):
                    value = _("Yes") if value else _("No")

            message_text += f"{field.name}: {value}\n"

        return message_text

    def additional_states(self):
        categories_list_menu = CategoryListMenu(self)
        return {self.States.ACTION: [categories_list_menu.handler]}

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_event$")]

    def save_object(self, obj, context, session=None):
        user_data = context.user_data
        translation = EventTranslation(lang='en')
        translation.name = user_data[self.menu_name]['name']
        translation.description = user_data[self.menu_name]['description']
        translation.place = user_data[self.menu_name]['place']
        translation.image = user_data[self.menu_name]['image']
        translation.organizers = user_data[self.menu_name]['organizers']
        obj.translations.append(translation)

        if not add_to_db([translation, obj], session):
            return self.conv_fallback(context)


class EventEditMenu(ArrowAddEditMenu):
    menu_name = "event_edit_menu"
    model = Event

    def entry(self, update, context):
        self._entry(update, context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('edit_event_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        event = self.parent.selected_object(context)
        if event:
            return DBSession.query(Event).filter(Event.id == event.id).first()
        else:
            self.parent.update_objects(context)
            self.parent.send_message(context)
            return ConversationHandler.END

    def fields(self, context):
        _ = context.user_data['_']
        fields = [self.Field('start_date', _("*Start date"), DateTimeConverter(), required=True),
                  self.Field('end_date', _("*End date"), DateTimeConverter(), required=True),
                  self.Field('category_id', _("*Category EN"), validators.String(), required=True),
                  ]
        return fields

    def message_text(self, context):
        user_data = context.user_data
        _ = user_data['_']

        message_text = self.message_top_text(context)
        available_fields = self._available_fields(context)
        active_field = user_data[self.menu_name]['active_field']

        for idx, field in enumerate(available_fields):
            message_text += (emojize(':play_button:') if active_field == idx else '       ')
            value = user_data[self.menu_name][field.param]
            if isinstance(value, str):
                value = value.replace("\_", "_").replace("\*", "*").replace("\[", "[").replace("\]", "]")
            if isinstance(value, datetime.datetime):
                value = value.strftime("%H:%M %d.%m.%Y")
            if value is None:
                value = _("Unknown")
            elif field.param == 'category_id':
                value = DBSession.query(Category).get(value).get_translation(user_data['user'].language_code).name
            elif field.is_document or field.is_photo:
                value = _("Uploaded")
            else:
                if isinstance(value, list):
                    value = ', '.join(value)
                elif isinstance(value, bool):
                    value = _("Yes") if value else _("No")

            message_text += f"{field.name}: {value}\n"

        return message_text

    def additional_states(self):
        categories_list_menu = CategoryListMenu(self)
        return {self.States.ACTION: [categories_list_menu.handler]}

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^edit_event$")]


class CategoryListMenu(BunchListMenu):
    menu_name = "category_list_menu"
    model = Category

    def query_objects(self, context):
        return DBSession.query(Category).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^field_category_id\\*$")]

    def message_text(self, context, objs):
        _ = context.user_data['_']

        if objs:
            message_text = _("Select category") + '\n'
        else:
            message_text = _("Create at least one category in admin menu.") + '\n'

        return message_text

    def object_buttons(self, context, objects):
        user = context.user_data['user']
        buttons = []
        for obj in objects:
            trans = obj.get_translation(user.language_code)
            if trans:
                buttons.append([InlineKeyboardButton(trans.name, callback_data=f'cat_{obj.id}')])

        return buttons

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.select_category, pattern=r'^cat_\d+$')]}

    def select_category(self, update, context):
        data = update.callback_query.data
        context.user_data[self.parent.menu_name]['category_id'] = int(data.replace('cat_', ''))
        self.parent.send_message(context)
        return ConversationHandler.END


class EventTranslationsMenu(OneListMenu):
    menu_name = 'event_translations_menu'

    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('event_translations_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        event = self.parent.selected_object(context)
        if event:
            return DBSession.query(EventTranslation).filter(EventTranslation.event_id == event.id).all()
        else:
            return []

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^event_translations$", pass_user_data=True)]

    def message_text(self, context, obj):
        _ = context.user_data['_']
        if obj:
            message_text = _("Event") + f" {obj.event_id} {_('translations')}:\n"
            message_text += _("Language") + f": {obj.lang.upper()}\n"
            message_text += _("Name") + f": {obj.name}\n"
            message_text += _("Description") + f": {obj.description}\n"
            message_text += _("Place") + f": {obj.place}\n"
            message_text += _("Image") + f": {_('Loaded') if obj.image else _('Not loaded')}" + '\n'
            message_text += _("Organizers") + f": {obj.organizers}\n"
        else:
            message_text = _("There is not translations yet") + '\n'

        return message_text

    def delete_ask(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('allow_delete_event_translation'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this action"), show_alert=True)
            return self.States.ACTION

        return super(EventTranslationsMenu, self).delete_ask(update, context)

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        exist_translations = [trans[0].upper() for trans in DBSession.query(EventTranslation.lang).filter(EventTranslation.event == self.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)

        if available_translations and user.has_permission('add_event_translation_menu_access'):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_event_trans"))
        if user.has_permission('allow_delete_event_translation') and len(exist_translations) > 1:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        if obj and user.has_permission('edit_event_translation_menu_access'):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"edit_event_trans"))
        return buttons

    def additional_states(self):
        event_trans_add_edit_menu = EventTranslationAddEditMenu(self)
        return {self.States.ACTION: [event_trans_add_edit_menu.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['_']
        return _("Translation deleted")


class EventTranslationAddEditMenu(ArrowAddEditMenu):
    menu_name = "event_translation_add_edit_menu"
    model = EventTranslation
    auto_switch_to_next_field_after_last_field = False

    def entry(self, update, context):
        self._entry(update, context)
        user = context.user_data['user']
        _ = context.user_data['_']
        self.load(context)
        if self.action(context) is self.Action.ADD and not user.has_permission('add_event_translation_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END
        if self.action(context) is self.Action.EDIT and not user.has_permission('edit_event_translation_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        return self.States.ACTION

    def variant_center_buttons(self, context):
        return []

    def query_object(self, context):
        cat_trans = self.parent.selected_object(context)
        if cat_trans and self.action(context) == self.Action.EDIT:
            return cat_trans
        else:
            return None

    def fields(self, context):
        _ = context.user_data['_']

        exist_translations = [trans[0].upper() for trans in DBSession.query(EventTranslation.lang).filter(EventTranslation.event == self.parent.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)
        if self.action(context) == self.Action.ADD:
            fields = [self.Field('lang', _("*Language"), validators.String(), variants=available_translations, required=True)]
        else:
            fields = []
        fields += [self.Field('name', _("*Name"), validators.String(), required=True),
                   self.Field('description', _("*Description"), validators.String(), required=True),
                   self.Field('place', _("*Place"), validators.String(), required=True),
                   self.Field('image', _("*Image"), validators.String(), is_photo=True, required=True),
                   self.Field('organizers', _("*Organizers"), validators.String(), required=True)]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_event_trans$", pass_user_data=True),
                CallbackQueryHandler(self.entry, pattern="^edit_event_trans$", pass_user_data=True)]

    def save_object(self, obj, context, session=None):
        obj.lang = obj.lang.lower()
        obj.event = self.parent.parent.selected_object(context)

        if not add_to_db(obj, session):
            return self.conv_fallback(context)

    def message_top_text(self, context):
        _ = context.user_data['_']
        if self.action(context) == self.Action.EDIT:
            text = "_____" + _("Edit event translation") + "_____\n"
            text += '       ' + _("Language") + f": {self.query_object(context).lang.upper()}\n"
        else:
            text = "___________\n"
        return text
