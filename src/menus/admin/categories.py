from botmanlib.menus import OneListMenu, ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db, group_buttons
from formencode import validators
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, ConversationHandler

from src.models import DBSession, Category, CategoryTranslation


class CategoriesMenu(OneListMenu):
    menu_name = 'categories_menu'

    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('categories_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        return DBSession.query(Category).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^categories$", pass_user_data=True)]

    def message_text(self, context, obj):
        _ = context.user_data['_']
        if obj:
            message_text = _("Category") + f" {obj.id}:\n"
            if obj.translations:
                for translation in obj.translations:
                    message_text += '  ' + _("Name") + f" {translation.lang.upper()}: {translation.name}" + '\n'
            else:
                message_text += '  ' + _("There is not translations yet") + '\n'
        else:
            message_text = _("There is not categories yet") + '\n'

        return message_text

    def delete_ask(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('allow_delete_category'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this action"), show_alert=True)
            return self.States.ACTION

        return super(CategoriesMenu, self).delete_ask(update, context)

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        if user.has_permission('add_category_menu_access'):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_category"))
        if user.has_permission('allow_delete_category') and obj:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))

        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        if obj:
            if user.has_permission('category_translations_menu_access'):
                buttons.append(InlineKeyboardButton(_("Translations"), callback_data='category_translations'))

        return group_buttons(buttons, 1)

    def additional_states(self):
        category_add_edit_menu = CategoryAddMenu(self)
        category_translations = CategoryTranslationsMenu(self)
        return {self.States.ACTION: [category_add_edit_menu.handler,
                                     category_translations.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['_']
        return _("Category deleted")


class CategoryAddMenu(ArrowAddEditMenu):
    menu_name = "category_add_menu"
    model = Category

    def entry(self, update, context):
        self._entry(update, context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('add_category_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        return None

    def fields(self, context):
        _ = context.user_data['_']
        fields = [self.Field('name', _("*Name EN"), validators.String(), required=True)]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_category$", pass_user_data=True)]

    def save_object(self, obj, context, session=None):
        translation = CategoryTranslation(lang='en', name=context.user_data[self.menu_name]['name'])
        obj.translations.append(translation)

        if not add_to_db([translation, obj], session):
            return self.conv_fallback(context)


class CategoryTranslationsMenu(OneListMenu):
    menu_name = 'categories_translations_menu'

    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('category_translations_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        cat = self.parent.selected_object(context)
        if cat:
            return DBSession.query(CategoryTranslation).filter(CategoryTranslation.category_id == cat.id).all()
        else:
            return []

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^category_translations$", pass_user_data=True)]

    def message_text(self, context, obj):
        _ = context.user_data['_']
        if obj:
            message_text = _("Category") + f" {obj.category_id} " + _("translations") + ':\n'
            message_text += _("Language") + f": {obj.lang.upper()}" + '\n'
            message_text += _("Name") + f": {obj.name}" + '\n'
        else:
            message_text = _("There is not translations yet") + '\n'

        return message_text

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        exist_translations = [trans[0].upper() for trans in DBSession.query(CategoryTranslation.lang).filter(CategoryTranslation.category == self.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)

        if available_translations and user.has_permission('add_category_translation_menu_access'):
            buttons.append(InlineKeyboardButton(_("Add"), callback_data="add_category_trans"))
        if user.has_permission('allow_delete_category_translation') and len(exist_translations) > 1:
            buttons.append(InlineKeyboardButton(_("Delete"), callback_data=f"delete_{self.menu_name}"))
        if obj and user.has_permission('edit_category_translation_menu_access'):
            buttons.append(InlineKeyboardButton(_("Edit"), callback_data=f"edit_category_trans"))
        return buttons

    def additional_states(self):
        category_trans_add_edit_menu = CategoryTranslationAddEditMenu(self)
        return {self.States.ACTION: [category_trans_add_edit_menu.handler]}

    def after_delete_text(self, context):
        _ = context.user_data['_']
        return _("Translation deleted")


class CategoryTranslationAddEditMenu(ArrowAddEditMenu):
    menu_name = "category_translation_add_edit_menu"
    model = CategoryTranslation
    auto_switch_to_next_field_after_last_field = False

    def entry(self, update, context):
        self._entry(update, context)
        user = context.user_data['user']
        _ = context.user_data['_']
        self.load(context)
        if self.action(context) is self.Action.ADD and not user.has_permission('add_category_translation_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END
        if self.action(context) is self.Action.EDIT and not user.has_permission('edit_category_translation_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.send_message(context)
        return self.States.ACTION

    def variant_center_buttons(self, context):
        return []

    def query_object(self, context):
        cat_trans = self.parent.selected_object(context)
        if cat_trans and self.action(context) == self.Action.EDIT:
            return cat_trans
        else:
            return None

    def fields(self, context):
        _ = context.user_data['_']

        exist_translations = [trans[0].upper() for trans in DBSession.query(CategoryTranslation.lang).filter(CategoryTranslation.category == self.parent.parent.selected_object(context)).all()]
        available_translations = []
        for lang in ["EN", "RU", "UK"]:
            if lang not in exist_translations:
                available_translations.append(lang)

        fields = [self.Field('lang', _("*Language"), validators.String(), variants=available_translations, required=True),
                  self.Field('name', _("*Name"), validators.String(), required=True)]
        return fields

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^add_category_trans$", pass_user_data=True),
                CallbackQueryHandler(self.entry, pattern="^edit_category_trans$", pass_user_data=True)]

    def save_object(self, obj, context, session=None):
        obj.lang = obj.lang.lower()
        obj.category = self.parent.parent.selected_object(context)

        if not add_to_db(obj, session):
            return self.conv_fallback(context)
