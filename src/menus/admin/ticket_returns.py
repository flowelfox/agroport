import datetime

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import add_to_db
from emoji import emojize
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, ConversationHandler

from src.models import TicketReturnRequest, DBSession


class TicketReturnsMenu(OneListMenu):
    menu_name = "ticket_returns_menu"
    model = TicketReturnRequest

    def entry(self, update, context):
        self._load(context)

        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('ticket_returns_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        context.user_data[self.menu_name]['show_processed'] = False
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        if context.user_data[self.menu_name]['show_processed']:
            return DBSession.query(TicketReturnRequest).filter(TicketReturnRequest.processed == True).order_by(TicketReturnRequest.create_date.desc()).all()
        else:
            return DBSession.query(TicketReturnRequest).filter(TicketReturnRequest.processed == False).order_by(TicketReturnRequest.create_date.asc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^ticket_returns$", pass_user_data=True)]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        if obj:
            event_trans = obj.ticket.event.get_translation(user.language_code)
            message_text = emojize(":recycling_symbol: ") + _("Request for a return") + f" # {obj.id}\n\n"

            message_text += "<b>" + _("Name") + f":</b> {obj.ticket.user.desired_name if obj.ticket.user else _('Unknown')}" + '\n'
            message_text += "<b>" + _("Card") + f":</b> {obj.card if obj.card else _('Unknown')}" + '\n'
            message_text += "<b>" + _("Reason") + f":</b> {obj.reason if obj.reason else _('Unknown')}" + '\n\n'
            message_text += "<b>" + _("Event name") + f":</b> {event_trans.name}" + '\n'
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            full_price = 0
            for subevent in obj.ticket.subevents:
                message_text += emojize(":small_blue_diamond: ") + subevent.get_translation(context.user_data['user'].language_code).name + " - " + (f"{subevent.price} {_('UAH')}" if subevent.price else _("Free")) + '\n'
                full_price += subevent.price
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            if full_price:
                message_text += emojize(":shopping_cart: ") + "<b>" + _("Price (UAH): {cost}").format(cost=full_price) + "</b>\n\n"
            else:
                message_text += emojize(":shopping_cart: ") + "<b>" + _("Price (UAH): Free") + "</b>\n\n"

            if obj.processed:
                message_text += emojize(":white_heavy_check_mark: ") + '<i>' + _("This ticket has already been processed by {date}").format(date=obj.processed_date.strftime("%H:%M %d.%m.%Y UTC")) + '</i>\n\n'

        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def mark_processed(self, update, context):
        _ = context.user_data['_']
        context.bot.answer_callback_query(update.callback_query.id, text=_("Request marked as processed"))
        req = self.selected_object(context)
        req.ticket.returned = True
        req.processed = True
        req.processed_date = datetime.datetime.utcnow()
        for subevent in req.ticket.subevents:
            subevent.ticket_limit += 1
        if not add_to_db(req.ticket.subevents + [req, req.ticket]):
            return self.conv_fallback(context)

        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def show_processed(self, update, context):
        context.user_data[self.menu_name]['show_processed'] = True
        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def hide_processed(self, update, context):
        context.user_data[self.menu_name]['show_processed'] = False
        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def center_buttons(self, context, obj=None):
        _ = context.user_data['_']
        if obj and not obj.processed:
            return [InlineKeyboardButton(_("Mark as processed"), callback_data="mark_processed")]

    def object_buttons(self, context, objects):
        _ = context.user_data['_']
        if context.user_data[self.menu_name]['show_processed']:
            return [[InlineKeyboardButton(_("Hide processed"), callback_data='hide_processed')]]
        else:
            return [[InlineKeyboardButton(_("Show processed"), callback_data='show_processed')]]

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "_______" + _("Request") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + "_______"

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.mark_processed, pattern='^mark_processed$'),
                                     CallbackQueryHandler(self.hide_processed, pattern='^hide_processed$'),
                                     CallbackQueryHandler(self.show_processed, pattern='^show_processed$')]}
