import datetime

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import group_buttons, get_settings
from emoji import emojize
from telegram import InlineKeyboardButton
from telegram.ext import CallbackQueryHandler, ConversationHandler

from src.models import DBSession, User, Ticket, Event
from src.settings import SETTINGS_FILE


class UsersInfoMenu(OneListMenu):
    menu_name = 'users_info_menu'
    model = User
    search_key_param = 'desired_name'

    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('users_info_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        if 'ticket_user_info' in update.callback_query.data:
            context.user_data[self.menu_name]['has_ticket'] = True
        else:
            context.user_data[self.menu_name]['has_ticket'] = False

        context.user_data[self.menu_name]['only_with_tickets'] = False
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        query = DBSession.query(User).filter(User.active == True)
        if context.user_data[self.menu_name]['has_ticket']:
            query = query.join(User.tickets).filter(Ticket.id == self.parent.tickets_info_menu.selected_object(context).id)
        if context.user_data[self.menu_name]['only_with_tickets']:
            query = query.join(User.tickets).filter(Ticket.returned == False)

        return query.order_by(User.join_date.desc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^users_info$"),
                CallbackQueryHandler(self.entry, pattern="^ticket_user_info$")]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        if obj:
            settings = get_settings(SETTINGS_FILE)
            regions = settings['regions'].get(user.language_code, settings['regions']['en'])
            regions['non_resident'] = emojize(':globe_with_meridians: ') + _("Non-resident")
            occupations = {
                'farmer': emojize(":sunflower: ") + _("Farmer"),
                'products_services_agro': emojize(":tractor: ") + _("Product/Services for Agro"),
                'other': emojize(":speech_balloon: ") + _("Other Activities")
            }

            message_text = f"____{_('Users')}____\n"
            message_text += _("User Telegram ID") + f": {obj.chat_id}\n"
            message_text += _("First Name") + f": {obj.first_name if obj.first_name else _('Unknown')}" + '\n'
            message_text += _("Last Name") + f": {obj.last_name if obj.last_name else _('Unknown')}" + '\n'
            message_text += _("Desired name") + f": {obj.desired_name if obj.desired_name else _('Unknown')}" + '\n'
            message_text += _("Username") + f": {'@' + obj.username if obj.username else _('Unknown')}" + '\n'
            message_text += _("Phone") + f": {obj.phone if obj.phone else _('Unknown')}" + '\n'
            message_text += _("Email") + f": {obj.email if obj.email else _('Unknown')}" + '\n'
            message_text += _("Region") + f": {regions.get(obj.region, _('Unknown')) if obj.region else _('Unknown')}\n"
            message_text += _("Company") + f": {obj.company if obj.company else _('Unknown')}" + '\n'
            message_text += _("Activity") + f": {occupations.get(obj.occupation, _('Unknown')) if obj.occupation else _('Unknown')}\n"
            message_text += _("Came from") + f": {obj.came_from if obj.came_from else _('Unknown')}" + '\n'
            message_text += _("User language") + f": {obj.language_code.upper() if obj.language_code else _('Unknown')}" + '\n'
            message_text += _("Active tickets") + f": {len(obj.active_tickets())}" + '\n'
        else:
            message_text = _("There is no users yet") + '\n'

        return message_text

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []

        if obj:
            if user.has_permission('user_tickets_menu_access') and len(obj.active_tickets()) > 0 and not context.user_data[self.menu_name]['has_ticket']:
                buttons.append(InlineKeyboardButton(_("Tickets"), callback_data="user_tickets_info"))
        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []

        if context.user_data[self.menu_name]['only_with_tickets']:
            buttons.append(InlineKeyboardButton(_("Show without tickets"), callback_data='only_with_tickets_switch'))
        else:
            buttons.append(InlineKeyboardButton(_("Show only with tickets"), callback_data='only_with_tickets_switch'))

        return group_buttons(buttons, 1)

    def switch_only_with_tickets(self, update, context):
        context.user_data[self.menu_name]['only_with_tickets'] = not context.user_data[self.menu_name]['only_with_tickets']
        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.switch_only_with_tickets, pattern='only_with_tickets_switch'),
                                     CallbackQueryHandler(self.goto_tickets, pattern='user_tickets_info'),
                                     ]}

    def goto_tickets(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "_______" + _("User") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + "_______"


class TicketsInfoMenu(OneListMenu):
    menu_name = 'tickets_info_menu'

    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('user_tickets_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        if update.callback_query.data == 'user_tickets_info':
            context.user_data[self.menu_name]['has_user'] = True
        else:
            context.user_data[self.menu_name]['has_user'] = False

        context.user_data[self.menu_name]['expired'] = False
        context.user_data[self.menu_name]['returned'] = False
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        query = DBSession.query(Ticket).join(Ticket.event).filter(Ticket.returned == False)
        if context.user_data[self.menu_name]['has_user']:
            query = query.filter(Ticket.user == self.parent.users_info_menu.selected_object(context))

        if not context.user_data[self.menu_name]['returned']:
            query = query.filter(Ticket.returned == False)

        if not context.user_data[self.menu_name]['expired']:
            query = query.filter(Event.end_date > datetime.datetime.now())

        return query.order_by(Ticket.create_date.desc()).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^tickets_info$"),
                CallbackQueryHandler(self.entry, pattern="^user_tickets_info$")]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        if obj:
            if obj.event:
                event_translation = obj.event.get_translation(user.language_code)
            else:
                event_translation = None

            message_text = emojize(":admission_tickets: ") + _("Ticket") + " # " + f"UA-000-{obj.id:04}" + '\n\n'
            message_text += _("Event information:") + '\n'
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            if obj.event is None:
                message_text += _("Error!") + '\n'
            else:
                message_text += f"<b>{event_translation.name if event_translation and event_translation.name else _('Unknown')}</b>\n"
                message_text += f"<i>{(event_translation.description[:50] + ('...' if len(event_translation.description) > 50 else '') if event_translation.description else _('Unknown'))}</i>\n\n"
                message_text += emojize(":eleven_o’clock: ") + "<b>" + _("Start") + f":</b> {obj.event.start_date.strftime('%H:%M %d.%m.%Y') if obj.event.start_date else _('Unknown')}\n"
                message_text += emojize(":one-thirty: ") + "<b>" + _("Finish") + f":</b> {obj.event.end_date.strftime('%H:%M %d.%m.%Y') if obj.event.end_date else _('Unknown')}\n\n"
                message_text += emojize(":round_pushpin: ") + "<b>" + _("Venue") + f":</b> {event_translation.place if event_translation.place else _('Unknown')}\n"
                message_text += emojize(":bellhop_bell: ") + "<b>" + _("Organizers") + f":</b> {event_translation.organizers if event_translation.organizers else _('Unknown')}\n\n"
                message_text += "<i>" + _("You want to visit and added for your ticket:") + "</i>\n"
                message_text += emojize(":wavy_dash:") * 10
                message_text += '\n'
                full_price = 0
                for subevent in obj.subevents:
                    message_text += emojize(":small_blue_diamond: ") + subevent.get_translation(context.user_data['user'].language_code).name + '\n'
                    full_price += subevent.price
                message_text += emojize(":wavy_dash:") * 10
                message_text += '\n'
                if full_price:
                    message_text += emojize(":shopping_cart: ") + "<b>" + _("Ticket cost {cost} UAH").format(cost=full_price) + "</b>\n\n"
                else:
                    message_text += emojize(":shopping_cart: ") + "<b>" + _("Ticket is free!") + "</b>\n\n"
        else:
            message_text = _("User has no tickets") + '\n'

        return message_text

    def center_buttons(self, context, obj=None):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []

        if obj:
            if user.has_permission('users_info_menu_access') and not context.user_data[self.menu_name]['has_user']:
                buttons.append(InlineKeyboardButton(_("User"), callback_data="ticket_user_info"))
        return buttons

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []

        if context.user_data[self.menu_name]['expired']:
            buttons.append(InlineKeyboardButton(_("Do not show expired"), callback_data='expired_switch'))
        else:
            buttons.append(InlineKeyboardButton(_("Show expired"), callback_data='expired_switch'))

        if context.user_data[self.menu_name]['returned']:
            buttons.append(InlineKeyboardButton(_("Do not show returned"), callback_data='returned_switch'))
        else:
            buttons.append(InlineKeyboardButton(_("Show returned"), callback_data='returned_switch'))

        return group_buttons(buttons, 1)

    def expired_switch(self, update, context):
        context.user_data[self.menu_name]['expired'] = not context.user_data[self.menu_name]['expired']
        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def returned_switch(self, update, context):
        context.user_data[self.menu_name]['returned'] = not context.user_data[self.menu_name]['returned']
        self.update_objects(context)
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.expired_switch, pattern='expired_switch'),
                                     CallbackQueryHandler(self.returned_switch, pattern='returned_switch'),
                                     CallbackQueryHandler(self.goto_user, pattern='ticket_user_info'),
                                     ]}

    def goto_user(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "_______" + _("Ticket") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + "_______"
