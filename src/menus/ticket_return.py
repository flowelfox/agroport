from botmanlib.menus import ArrowAddEditMenu
from botmanlib.menus.helpers import add_to_db
from emoji import emojize
from formencode import validators
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, ConversationHandler

from src.models import TicketReturnRequest


class TicketReturnMenu(ArrowAddEditMenu):
    menu_name = "ticket_return_menu"
    model = TicketReturnRequest
    show_arrows = False

    def entry(self, update, context):
        self._entry(update, context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('ticket_return_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def query_object(self, context):
        return None

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^return_ticket$", pass_user_data=True)]

    def fields(self, context):
        _ = context.user_data['_']
        fields = [self.Field('reason', _("1.Reason*"), validators.String(), default=_("Sorry, i can't visit this event"), required=True)]
        if not self.parent.selected_object(context).is_free():
            fields.append(self.Field('card', _("2.Credit card*"), validators.CreditCardValidator(),required=True, before_validate=lambda text: {'ccNumber': text, 'ccType': 'visa' if text.startswith('4') else 'mastercard'}, after_validate=lambda value: value['ccNumber']))
        return fields

    def reset_button(self, context):
        return None

    def save_object(self, obj, context, session=None):
        ticket = self.parent.selected_object(context)
        if ticket.is_free():
            ticket.returned = True
            for subevent in ticket.subevents:
                subevent.ticket_limit += 1
            add_to_db(ticket.subevents + [ticket], session)
        else:
            ticket.return_request = obj
            add_to_db([obj, ticket], session)

    def save(self, update, context):
        _ = context.user_data['_']

        if not self.check_fields(context):
            self.delete_interface(context)
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please fill required fields."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION
        self.delete_interface(context, 'variations_interface')

        self.update_object(context)

        return self.after_save(update, context)

    def after_save(self, update, context):
        _ = context.user_data['_']
        user = context.user_data['user']
        ticket = self.parent.selected_object(context)
        if ticket.is_free():
            message_text = emojize(":wastebasket: ") + _("This ticket was free and it was successfully removed.")
        else:
            message_text = _("Your request has been sent. It may take 1-3 work days to process your request.")
        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("My tickets"), callback_data=f'back_{self.menu_name}')]]

        self.send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.ACTION

    def save_button(self, context):
        _ = context.user_data['_']
        return InlineKeyboardButton(emojize(":incoming_envelope: ") + _("Send"), callback_data=f"save_{self.menu_name}")

    def back_button(self, context):
        _ = context.user_data['_']
        return InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data=f"back_{self.menu_name}")

    def message_top_text(self, context):
        _ = context.user_data['_']
        message_text = "<b>" + _("RETURN TICKET") + "</b>\n"
        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'
        message_text += emojize(":warning: ") + _("Please fill form below to send return request") + '\n\n'

        return message_text

    def message_bottom_text(self, context):
        _ = context.user_data['_']
        message_text = emojize(":wavy_dash:") * 10
        message_text += '\n'
        message_text += "<i>*  " + _("Required fields") + "</i>"
        return message_text
