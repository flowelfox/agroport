import datetime
import enum
import io
import re
import smtplib
import ssl
from email.encoders import encode_base64
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from io import BytesIO
from os import path
from textwrap import wrap

import qrcode
from PyPDF2 import PdfFileReader, PdfFileWriter
from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import generate_regex_handlers, to_state, add_to_db
from emoji import emojize, demojize
from formencode import validators, Invalid
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.menus.extend_ticket import ExtendTicketMenu
from src.menus.ticket_return import TicketReturnMenu
from src.models import Ticket, EventTranslation, User
from src.settings import RESOURCES_FOLDER, SENDER_EMAIL, SENDER_SERVER, SENDER_SERVER_PORT, SENDER_PASSWORD, SENDER_USERNAME


class TicketsMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1
        QR_CODE = 2
        DOWNLOAD_PDF = 3
        MORE = 4
        SEND_EMAIL = 5
        SET_EMAIL = 6

    menu_name = "tickets_menu"
    model = Ticket
    auto_hide_arrows = True

    def entry(self, update, context):
        user = self.prepare_user(User, update, context)
        _ = context.user_data['_']
        self._load(context)

        if not user.has_permission('my_tickets_menu_access'):
            self.bot.send_message(chat_id=user.chat_id, text=_("You were restricted to use this menu"))
            return ConversationHandler.END

        self.delete_interface(context, interface_name='main')

        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        user = context.user_data['user']
        return user.active_tickets()

    def entry_points(self):
        return generate_regex_handlers("My tickets", self.entry, right_text=' \(\d+\)', left_text=emojize(":admission_tickets: ")) + \
               [CallbackQueryHandler(self.entry, pattern='^my_tickets$')]

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        if obj:
            if obj.event:
                event_translation = obj.event.get_translation(user.language_code)
            else:
                event_translation = None
            message_text = emojize(":admission_tickets: ") + _("Ticket") + " # " + f"UA-000-{obj.id:04}" + '\n\n'
            message_text += _("Event information:") + '\n'
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            if obj.event is None:
                message_text += _("Error!") + '\n'
            else:
                message_text += f"<b>{event_translation.name if event_translation and event_translation.name else _('Unknown')}</b>\n"
                message_text += f"<i>{(event_translation.description[:50] + ('...' if len(event_translation.description) > 50 else '') if event_translation.description else _('Unknown'))}</i>\n\n"
                message_text += emojize(":eleven_o’clock: ") + "<b>" + _("Start") + f":</b> {obj.event.start_date.strftime('%H:%M %d.%m.%Y') if obj.event.start_date else _('Unknown')}\n"
                message_text += emojize(":one-thirty: ") + "<b>" + _("Finish") + f":</b> {obj.event.end_date.strftime('%H:%M %d.%m.%Y') if obj.event.end_date else _('Unknown')}\n\n"
                message_text += emojize(":round_pushpin: ") + "<b>" + _("Venue") + f":</b> {event_translation.place if event_translation.place else _('Unknown')}\n"
                message_text += emojize(":bellhop_bell: ") + "<b>" + _("Organizers") + f":</b> {event_translation.organizers if event_translation.organizers else _('Unknown')}\n\n"
                message_text += "<i>" + _("You want to visit and added for your ticket:") + "</i>\n"
                message_text += emojize(":wavy_dash:") * 10
                message_text += '\n'
                full_price = 0
                for subevent in obj.subevents:
                    message_text += emojize(":small_blue_diamond: ") + subevent.get_translation(context.user_data['user'].language_code).name + '\n'
                    full_price += subevent.price
                message_text += emojize(":wavy_dash:") * 10
                message_text += '\n'
                if full_price:
                    message_text += emojize(":shopping_cart: ") + "<b>" + _("Your ticket cost {cost} UAH").format(cost=full_price) + "</b>\n\n"
                else:
                    message_text += emojize(":shopping_cart: ") + "<b>" + _("Your ticket is free!") + "</b>\n\n"

            if obj.return_request:
                message_text += '\n<i>' + _("You requested return for this ticket.\nTicket will be removed when request is processed.") + '</i>\n'
        else:
            message_text = _("You have no ticket yet. You can select it in {emoji} <b>Event Section</b>").format(lq='\u00AB', rq='\u00BB', emoji=emojize(":tear-off_calendar:")) + '\n'

        return message_text

    def back_button(self, context):
        _ = context.user_data['_']
        return InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data=f"back_{self.menu_name}")

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        if obj:
            buttons.append([InlineKeyboardButton(emojize(":information: ") + _("More Info"), callback_data='ticket_more')])
            if obj.return_request is None:
                buttons.append([InlineKeyboardButton(emojize(":envelope_with_arrow: ") + _("Send by Email"), callback_data='send_to_email')])
                buttons.append([InlineKeyboardButton(emojize(":mobile_phone: ") + _("Show QR-code"), callback_data='show_qr_code')])
                buttons.append([InlineKeyboardButton(emojize(":printer: ") + _("PDF Ticket"), callback_data='download_pdf')])
                buttons.append([InlineKeyboardButton(emojize(":heavy_plus_sign: ") + _("Add event"), callback_data='extend_ticket')])
            if obj.event.start_date - datetime.timedelta(days=1) > datetime.datetime.utcnow() and obj.return_request is None:
                buttons.append([InlineKeyboardButton(emojize(":wastebasket: ") + _("Return"), callback_data='return_ticket')])

        return buttons

    def more(self, update, context):
        _ = context.user_data['_']
        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_ticket')]]

        translation = self.selected_object(context).event.get_translation(context.user_data['user'].language_code)
        if translation is None:
            translation = EventTranslation(name=_("Unknown"), description=_("Unknown"))

        message_text = "<b>" + (translation.name if translation.name else _("Unknown")) + "</b>\n"
        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'
        message_text += "<i>" + (translation.description if translation.description else _("Unknown")) + "</i>"
        self.send_or_edit(context, chat_id=context.user_data['user'], text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.MORE

    def show_qr_code(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        if not user.has_permission('allow_view_ticket_qr_code'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this action"), show_alert=True)
            return self.States.ACTION

        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_ticket')]]
        ticket = self.selected_object(context)
        with BytesIO() as output:
            data = f"{ticket.id}|{ticket.user.id}|{ticket.event.id}|{'-'.join([str(subevent.id) for subevent in ticket.subevents])}"
            image = qrcode.make(data)
            image.save(output, format="PNG")
            output.seek(0)
            self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, photo=output, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.QR_CODE

    def download_pdf(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        if not user.has_permission('allow_download_ticket'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this action"), show_alert=True)
            return self.States.ACTION

        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_ticket')]]
        ticket = self.selected_object(context)

        result_pdf = self.create_pdf_ticket(ticket)
        with BytesIO() as output:
            result_pdf.write(output)
            output.seek(0)

            filename = _("Ticket to {}").format(ticket.event.get_translation(ticket.user.language_code).name) + '.pdf'
            self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, document=output, filename=filename, reply_markup=InlineKeyboardMarkup(buttons))

        return self.States.DOWNLOAD_PDF

    def ask_email(self, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_ticket')]]
        self.send_or_edit(context, chat_id=user.chat_id, text=emojize(':writing_hand: ') + _("Please enter your Email and send me."), reply_markup=InlineKeyboardMarkup(buttons))

    def set_email(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        try:
            val = validators.Email()
            value = val.to_python(update.effective_message.text)

            user.email = value
            if not add_to_db(user):
                return self.conv_fallback(context)
            self.delete_interface(context)
            self.send_to_email(update, context)

        except Invalid:
            buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_ticket')]]
            self.send_or_edit(context, chat_id=user.chat_id, text=_("Wrong input. Please enter your email."), reply_markup=InlineKeyboardMarkup(buttons))
            return self.States.SET_EMAIL

    def send_to_email(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        if not user.has_permission('allow_email_ticket'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this action"), show_alert=True)
            return self.States.ACTION

        if not user.email:
            self.ask_email(context)
            return self.States.SET_EMAIL

        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_ticket')]]
        self.send_or_edit(context, chat_id=user.chat_id, text=_("Ticket was send to your email"), reply_markup=InlineKeyboardMarkup(buttons))

        ticket = self.selected_object(context)

        filename = _("Ticket to {}").format(ticket.event.get_translation(ticket.user.language_code).name) + '.pdf'

        message = MIMEMultipart()
        message["Subject"] = _("Your PDF ticket on AgriEvent")
        message["From"] = SENDER_EMAIL
        message["To"] = user.email

        content = _("Congratulations!") + "\n\n"
        content += "<b>" + _("You have successfully ordered a PDF-ticket for an agro-event. He is waiting for you in the attached file {emoji}").format(emoji=emojize(":incoming_envelope:")) + "</b>\n\n"
        content += "" + _("<i>It is also available in the {emoji} </i><b>My tickets</b><i> section of the main menu</i>") + " t.me/AgriEventBot" + '</i>\n'
        content += emojize(":wavy_dash:") * 10
        content += '\n'
        content += emojize(":printer: ") + _("You can print it or use the electronic version") + '\n'
        content += emojize(":bell: ") + _("We'll remind you about the event a few days before the event") + '\n\n'
        content += "<b>" + _("Sent with {emoji} from AgriEventBot").format(emoji=emojize(":heart_suit:")) + "</b>\n"
        content += emojize(":wavy_dash:") * 10
        content += '\n'
        content += f'<a href="https://t.me/AgriEventBot">Telegram</a>' + "  |  "
        content += f'<a href="https://www.instagram.com/agri.event/">Instagram</a>' + "  |  "
        content += f'<a href="https://www.facebook.com/agrievent/">Facebook</a>'

        html = """\
                   <html>
                     <body>
                       <p>
                           {content}
                       </p>
                     </body>
                   </html>
                   """.format(content=content.replace('\n', '<br/>'))

        html_part = MIMEText(html, "html")
        message.attach(html_part)

        result_pdf = self.create_pdf_ticket(ticket)
        with BytesIO() as output:
            result_pdf.write(output)
            output.seek(0)
            attachedfile = MIMEApplication(output.read(), _subtype="pdf", _encoder=encode_base64)
            attachedfile.add_header('Content-Disposition', 'attachment', filename=filename)
            message.attach(attachedfile)

        # f = open(path.join(RESOURCES_FOLDER, "message.eml"), "wb")
        # f.write(bytes(message.as_string(), 'utf-8'))
        # f.close()

        # send mail
        ssl_context = ssl.create_default_context()
        with smtplib.SMTP_SSL(SENDER_SERVER, SENDER_SERVER_PORT, context=ssl_context) as server:
            server.login(SENDER_USERNAME, SENDER_PASSWORD)
            server.sendmail(SENDER_EMAIL, user.email, message.as_string())

        if update.callback_query:
            self.bot.answer_callback_query(update.callback_query.id)
        return self.States.SEND_EMAIL

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "_______" + _("Ticket") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + "_______"

    def back(self, update, context):
        self.delete_interface(context)
        self.parent.send_message(context)
        return ConversationHandler.END

    def additional_states(self):
        extend_ticket_menu = ExtendTicketMenu(self)
        ticket_return_menu = TicketReturnMenu(self)
        return {self.States.ACTION: [CallbackQueryHandler(self.show_qr_code, pattern='^show_qr_code$'),
                                     CallbackQueryHandler(self.download_pdf, pattern='^download_pdf$'),
                                     CallbackQueryHandler(self.send_to_email, pattern='^send_to_email$'),
                                     CallbackQueryHandler(self.more, pattern='^ticket_more'),
                                     extend_ticket_menu.handler,
                                     ticket_return_menu.handler,
                                     MessageHandler(Filters.all, to_state(self.States.ACTION))],
                self.States.QR_CODE: [CallbackQueryHandler(self.entry, pattern='^back_to_ticket$'),
                                      MessageHandler(Filters.all, to_state(self.States.QR_CODE))],
                self.States.DOWNLOAD_PDF: [CallbackQueryHandler(self.entry, pattern='^back_to_ticket$'),
                                           MessageHandler(Filters.all, to_state(self.States.DOWNLOAD_PDF))],
                self.States.SEND_EMAIL: [CallbackQueryHandler(self.entry, pattern='^back_to_ticket$'),
                                         MessageHandler(Filters.all, to_state(self.States.SEND_EMAIL))],
                self.States.SET_EMAIL: [CallbackQueryHandler(self.entry, pattern='^back_to_ticket$'),
                                        MessageHandler(Filters.text, self.set_email),
                                        MessageHandler(Filters.all, to_state(self.States.SET_EMAIL))],
                self.States.MORE: [CallbackQueryHandler(self.entry, pattern='^back_to_ticket$'),
                                   MessageHandler(Filters.all, to_state(self.States.MORE))]
                }

    def create_pdf_ticket(self, ticket):
        user = ticket.user
        event = ticket.event
        lang = user.language_code if user.language_code in ['en', 'ru', 'uk'] else "en"
        event_trans = event.get_translation(lang)

        overlay = io.BytesIO()
        can = canvas.Canvas(overlay, pagesize=A4, bottomup=0)

        # qr code
        with io.BytesIO() as output:
            data = f"{ticket.id}|{ticket.user.id}|{ticket.event.id}|{'-'.join([str(subevent.id) for subevent in ticket.subevents])}"
            image = qrcode.make(data, border=0)
            image.save(output, format="PNG")
            output.seek(0)
            can.drawInlineImage(image, 255, 55, 80, 80, anchorAtXY=True)

        # event name
        can.setFont("Arial-Bold", 11)
        base_event_name_y = 56
        event_name_list = wrap(re.sub(':.*?:', "", demojize(event_trans.name)), 30)
        for idx, line in enumerate(event_name_list):
            can.drawString(36, (base_event_name_y - (len(event_name_list) - 1) * 14) + idx * 14, line)

        # event organizer
        can.setFont("Arial", 11)
        base_event_organizer_y = 70
        event_organizer_list = wrap(re.sub(':.*?:', "", demojize(event_trans.organizers)), 32)
        for idx, line in enumerate(event_organizer_list):
            can.drawString(36, (base_event_organizer_y + (idx * 13)), line)

        # user name
        can.setFont("Arial-Bold", 19)
        base_user_name_y = 133
        name_list = wrap(re.sub(':.*?:', "", demojize(user.name)), 15)
        for idx, line in enumerate(name_list):
            can.drawString(36, (base_user_name_y - (len(name_list) - 1) * 23) + idx * 23, line)

        # user company
        can.setFont("Arial", 11)
        can.drawString(36, 156, re.sub(':.*?:', "", demojize(user.company)) if user.company else "")

        # ticket id
        can.setFont("Arial-Bold", 14)
        can.drawString(92, 233, f"UA-000-{ticket.id:04}")

        # event name
        can.setFont("Arial-Bold", 10)
        if lang.lower() == 'en':
            event_name_x = 100
        elif lang.lower() == 'ru':
            event_name_x = 160
        elif lang.lower() == 'uk':
            event_name_x = 110
        else:
            event_name_x = 100
        can.drawString(event_name_x, 258, re.sub(':.*?:', "", demojize(event_trans.name)))

        # event start date
        can.setFont("Arial", 9)
        can.drawString(108, 280, event.start_date.strftime("%H:%M %d.%m.%Y"))

        # event end date
        can.setFont("Arial", 9)
        can.drawString(108, 292, event.end_date.strftime("%H:%M %d.%m.%Y"))

        # event place
        can.setFont("Arial", 9)
        can.drawString(288, 280, re.sub(':.*?:', "", demojize(event_trans.place)))

        # event organizer
        can.setFont("Arial", 9)
        bottom_event_organizer_base_y = 292
        bottom_event_organizer_list = wrap(re.sub(':.*?:', "", demojize(event_trans.organizers)), 60)
        for idx, line in enumerate(bottom_event_organizer_list):
            can.drawString(288, (bottom_event_organizer_base_y + (idx * 10)), line)

        subevent_y = 388
        for subevent in ticket.subevents:
            # subevent date
            can.setFont("Arial", 8)
            can.drawString(36, subevent_y, subevent.date.strftime("%H:%M %d.%m.%Y"))

            # subevent place
            can.setFont("Arial", 8)
            can.drawString(396, subevent_y, re.sub(':.*?:', "", demojize(subevent.get_translation(lang).place)))

            # subevent name
            can.setFont("Arial", 8)
            subevent_name_list = wrap(re.sub(':.*?:', "", demojize(subevent.get_translation(lang).name)), 60)
            for idx, subevent_name in enumerate(subevent_name_list):
                if idx > 0:
                    subevent_y += 9
                can.drawString(108, subevent_y, subevent_name)
            subevent_y += 10

        can.save()
        overlay.seek(0)

        overlay_pdf = PdfFileReader(overlay, strict=False)
        # read template PDF
        template_pdf = PdfFileReader(open(path.join(RESOURCES_FOLDER, f'AIB_Ticket_{lang.upper()}.pdf'), 'rb'), strict=False)

        result_pdf = PdfFileWriter()
        template_page = template_pdf.getPage(0)
        template_page.mergePage(overlay_pdf.getPage(0))
        result_pdf.addPage(template_page)

        return result_pdf
