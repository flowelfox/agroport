import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import to_state, add_to_db, make_payment, check_payment
from emoji import emojize
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, Chat, User as TUser, CallbackQuery
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters, ConversationHandler

from src.models import DBSession, Subevent
from src.settings import WEBHOOK_ENABLE


class ExtendTicketMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1
        MORE = 2
        PAYMENT = 3

    menu_name = "extend_ticket_menu"
    model = Subevent
    disable_web_page_preview = False

    def entry(self, update, context):
        self._load(context)

        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('ticket_extend_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        ticket = self.parent.selected_object(context)
        context.user_data[self.menu_name]['extend_cart'] = []
        for subevent in ticket.subevents:
            context.user_data[self.menu_name]['extend_cart'].append(subevent)
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)

        return self.States.ACTION

    def query_objects(self, context):
        ticket = self.parent.selected_object(context)
        return DBSession.query(Subevent).filter(Subevent.event_id == ticket.event.id).order_by(Subevent.date).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^extend_ticket$")]

    def object_buttons(self, context, obj):
        _ = context.user_data['_']
        buttons = []
        if obj:
            ticket = self.parent.selected_object(context)
            if obj not in context.user_data[self.menu_name]['extend_cart']:
                if obj.ticket_limit >= 1:
                    buttons.append([InlineKeyboardButton(emojize(":heavy_plus_sign: ") + _("Add"), callback_data='add_subevent')])
            if obj not in ticket.subevents and obj in context.user_data[self.menu_name]['extend_cart']:
                buttons.append([InlineKeyboardButton(emojize(":cross_mark: ") + _("Remove from ticket"), callback_data='remove_subevent')])

            buttons.append([InlineKeyboardButton(emojize(":information: ") + _("More Info"), callback_data='subevent_more')])
            buttons.append([InlineKeyboardButton(emojize(":white_heavy_check_mark: ") + _("Confirmation / Payment"), callback_data='goto_payment')])
        return buttons

    def more(self, update, context):
        _ = context.user_data['_']
        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_extend_ticket')]]

        translation = self.selected_object(context).get_translation(context.user_data['user'].language_code)
        self.send_or_edit(context, chat_id=context.user_data['user'], text=translation.description, reply_markup=InlineKeyboardMarkup(buttons))
        return self.States.MORE

    def add_to_cart(self, update, context):
        _ = context.user_data['_']
        context.user_data[self.menu_name]['extend_cart'].append(self.selected_object(context))
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id, text=_("Added to ticket"))
        return self.States.ACTION

    def remove_from_cart(self, update, context):
        _ = context.user_data['_']
        if self.selected_object(context) in context.user_data[self.menu_name]['extend_cart']:
            context.user_data[self.menu_name]['extend_cart'].remove(self.selected_object(context))
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id, text=_("Removed from ticket"))
        return self.States.ACTION

    def payment_info(self, update, context):
        _ = context.user_data['_']
        user = context.user_data['user']
        ticket = self.parent.selected_object(context)

        if not context.user_data[self.menu_name]['extend_cart'] or len(context.user_data[self.menu_name]['extend_cart']) == len(ticket.subevents):
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please select new places to visit"))
            return self.States.ACTION

        event_translation = self.parent.selected_object(context).event.get_translation(context.user_data['user'].language_code)
        message_text = "<b>" + _("You reordered ticket to {event_name} event.").format(event_name=event_translation.name) + '</b>\n\n'
        message_text += _("You want to visit and added for your ticket:") + '\n'
        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'
        full_price = 0
        for subevent in context.user_data[self.menu_name]['extend_cart']:
            message_text += emojize(":small_blue_diamond: ") + subevent.get_translation(context.user_data['user'].language_code).name
            if subevent in ticket.subevents:
                message_text += ' ' + _("(Was in ticket)")
            else:
                full_price += subevent.price
            message_text += '\n'

        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'

        if full_price > 0:
            order_description = _("Reordering ticket to {}").format(event_translation.name)
            res_data = make_payment(full_price * 100, order_description, lang=user.language_code, response_url=self.bot.link)
            if not res_data:
                return self.conv_fallback(context)
            link = res_data['checkout_url']

            message_text += '\n' + _("You must pay by this link:\n")
            message_text += link + '\n'
            message_text += _("Payment will process automatically, it can take 5-10 seconds.")
            buttons = []
            job_context = {'order_id': res_data['order_id'],
                           'link': link,
                           'user_data': context.user_data}

            context.job_queue.run_repeating(self.job_after_pay, interval=5, context=job_context, first=10)
        else:
            message_text += '\n' + emojize(":shopping_cart:") + "<b>" + _("You ticket is free!") + "</b>"
            buttons = [[InlineKeyboardButton(emojize(":white_heavy_check_mark: ") + _("Confirm"), callback_data='confirm_free_ticket')]]
        buttons.append([InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_subevents')])

        self.send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.PAYMENT

    def _payment_text(self, context):
        _ = context.user_data['_']
        message_text = emojize(":white_heavy_check_mark:")
        message_text += "<b>" + _("Congratulations!\nYour ticket has been successfully updated") + '</b>\n\n'
        message_text += _("<i>Now it is available in the {emoji} </i><b>My tickets</b><i> section of the main menu</i>").format(emoji=emojize(":admission_tickets:")) + "\n\n"
        message_text += "<b>" + _("What's next:") + "</b>\n"
        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'
        message_text += emojize(":printer: ") + _("You can print it or use the electronic version") + "\n"
        message_text += emojize(":bell: ") + _("We'll remind you about the event a few days before the event")
        return message_text

    def confirm_payment(self, update, context):
        _ = context.user_data['_']
        self.update_ticket(context)

        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Main menu"), callback_data='start')]]

        self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=self._payment_text(context), reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)

        return self.States.PAYMENT

    def update_ticket(self, context):
        ticket = self.parent.selected_object(context)
        full_price = 0
        ticket.subevents = []
        for subevent in context.user_data[self.menu_name]['extend_cart']:
            ticket.subevents.append(subevent)
            full_price += subevent.price
            subevent.ticket_limit -= 1

        ticket.price = full_price
        if not add_to_db(ticket.subevents + [ticket]):
            return self.conv_fallback(context)
        context.user_data[self.menu_name]['extend_cart'] = []

    def job_after_pay(self, context):
        job = context.job
        if job.context is None:
            job.schedule_removal()
            return

        context.user_data = job.context['user_data']
        _ = context.user_data['_']

        try:
            data = check_payment(job.context['order_id'])
        except TimeoutError as e:
            data = None
            self.logger.warning(str(e))

        if not data or data == 'error':
            context.bot.send_message(chat_id=context.user_data['user'].chat_id, text=_("An error has occurred you do not need to confirm payment."))
            job.schedule_removal()

        elif data == 'expired':
            message_text = emojize(":prohibited: ") + _("You have not extended your ticket") + '\n'
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            message_text += _("Time of payment by the link\n{link}\nwent out.") + '\n\n'
            message_text += _("To get extend ticket, go back to the ticket page and try again for the extend and payment")
            context.bot.send_message(chat_id=context.user_data['user'].chat_id, text=message_text.format(link=job.context['link']))
            context.user_data[self.menu_name]['extend_cart'].clear()
            job.schedule_removal()

        elif data == 'success':
            self.update_ticket(context)
            message_text = self._payment_text(context)
            message_text += "\n\n<i>" + _("You will be redirected to main menu after 5 seconds") + "</i>"
            self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, parse_mode=self.parse_mode)
            context.job_queue.run_once(self.goto_start, 5, context=context.user_data, name=f"goto_start_after_{job.context['order_id']}")
            job.schedule_removal()

    def goto_start(self, context):
        context.user_data = context.job.context
        update = Update(0)
        user = context.user_data['user']
        update._effective_user = TUser(user.chat_id, user.first_name, False, user.last_name, user.username, user.language_code, bot=context.bot)
        update._effective_chat = Chat(user.chat_id, Chat.PRIVATE)
        # update.message = Message(0, update.effective_user, datetime.datetime.utcnow(), update.effective_chat, text='/start', bot=context.bot)
        update._effective_message = None
        update.callback_query = CallbackQuery(0, update.effective_user, update.effective_chat, bot=context.bot, data='start')
        context.update_queue.put(update)

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        if obj:
            ticket = self.parent.selected_object(context)
            translation = obj.get_translation(user.language_code)

            message_text = f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += emojize(":heavy_plus_sign: ") + "<i>" + _("Please select events that you want to visit and add its for your Ticket:") + '</i>\n'
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n\n'
            message_text += f"<b>{(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += f"<i>{(translation.description[:50] + ('...' if len(translation.description) > 50 else '') if translation.description else _('Unknown'))}</i>\n\n"
            message_text += emojize(":eleven_o’clock: ") + "<b>" + _("Start") + f":</b> {obj.date.strftime('%H:%M %d.%m.%Y') if obj.date else _('Unknown')}\n"
            message_text += emojize(":round_pushpin: ") + "<b>" + _("Place") + f":</b> {translation.place if translation.place else _('Unknown')}\n"
            message_text += emojize(":bellhop_bell: ") + "<b>" + _("Organizers") + f":</b> {translation.organizers if translation.organizers else _('Unknown')}\n"
            message_text += emojize(":shopping_cart:") + "<b>" + _("Price (UAH)") + f":</b> {obj.price if obj.price else _('Free')}\n"
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            if obj in context.user_data[self.menu_name]['extend_cart'] and obj in ticket.subevents:
                message_text += emojize(":white_heavy_check_mark: ") + '<b>' + _("You have already added this event to the ticket") + '</b>\n'
            elif obj.ticket_limit <= 0:
                message_text += "<i>" + _("Sorry, we already sell all Tickets to this event.") + "</i>\n"
        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "(" + _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

    def back_button(self, context):
        _ = context.user_data['_']
        return InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data=f"back_{self.menu_name}")

    def additional_states(self):
        return {self.States.ACTION: [CallbackQueryHandler(self.more, pattern='^subevent_more'),
                                     CallbackQueryHandler(self.add_to_cart, pattern='^add_subevent'),
                                     CallbackQueryHandler(self.remove_from_cart, pattern='^remove_subevent'),
                                     CallbackQueryHandler(self.payment_info, pattern='^goto_payment')],
                self.States.MORE: [CallbackQueryHandler(self.entry, pattern='^back_to_extend_ticket$'),
                                   MessageHandler(Filters.all, to_state(self.States.MORE))],
                self.States.PAYMENT: [CallbackQueryHandler(self.entry, pattern='^back_to_extend_ticket$'),
                                      CallbackQueryHandler(self.confirm_payment, pattern='^confirm_free_ticket$'),
                                      MessageHandler(Filters.all, to_state(self.States.PAYMENT))]
                }
