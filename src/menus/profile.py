import enum

from botmanlib.menus import ArrowAddEditMenu
from botmanlib.menus.helpers import generate_regex_handlers, get_settings
from botmanlib.menus.validators import PhoneNumber
from emoji import emojize
from formencode import validators
from telegram import InlineKeyboardButton, TelegramError, ReplyKeyboardMarkup, KeyboardButton
from telegram.ext import ConversationHandler, MessageHandler, Filters, CallbackQueryHandler

from src.models import DBSession, User
from src.settings import SETTINGS_FILE


class ProfileMenu(ArrowAddEditMenu):
    class States(enum.Enum):
        ACTION = 1

    menu_name = 'profile_menu'
    model = User
    show_reset_field = True
    reset_to_default = True
    use_html_text = True
    force_action = ArrowAddEditMenu.Action.EDIT
    variants_per_page = 10
    resend_after_each_field = True
    show_field_selectors = False

    def entry(self, update, context):
        user = self.prepare_user(User, update, context)
        _ = context.user_data['_']

        self._entry(update, context)
        if not user.has_permission('registration_menu_access'):
            self.bot.send_message(chat_id=user.chat_id, text=_("You were restricted to use this menu"))
            return ConversationHandler.END
        if self.get_interface(context, 'main') and update.callback_query is None:
            self.delete_interface(context, 'main')
            buttons = [[KeyboardButton(emojize(":Ukraine:") + emojize(":Russia:") + emojize(":United_Kingdom:"))]]
            self.send_or_edit(context, interface_name='main', chat_id=user.chat_id, text=_("Select action:"), reply_markup=ReplyKeyboardMarkup(buttons, resize_keyboard=True))

        self.load(context)
        self.send_message(context)
        return self.States.ACTION

    def load(self, context):
        user_data = context.user_data
        user = user_data['user']
        _ = user_data['_']

        settings = get_settings(SETTINGS_FILE)
        user_data[self.menu_name]['regions'] = settings['regions'].get(user.language_code, settings['regions'].get('en', []))
        user_data[self.menu_name]['regions']['non_resident'] = emojize(':globe_with_meridians: ') + _("Non-resident")

        user_data[self.menu_name]['occupations'] = {
            'farmer': emojize(":sunflower: ") + _("Farmer"),
            'products_services_agro': emojize(":tractor: ") + _("Product/Services for Agro"),
            'other': emojize(":speech_balloon: ") + _("Other Activities")
        }
        super(ProfileMenu, self).load(context)

        if not user_data[self.menu_name]['desired_name']:
            user_data[self.menu_name]['desired_name'] = user.name
            user_data[self.menu_name]['active_field'] = 1

        if 'region' in user_data[self.menu_name] and user_data[self.menu_name]['region']:
            user_data[self.menu_name]['region'] = user_data[self.menu_name]['regions'].get(user_data[self.menu_name]['region'], None)

        if 'occupation' in user_data[self.menu_name] and user_data[self.menu_name]['occupation']:
            user_data[self.menu_name]['occupation'] = user_data[self.menu_name]['occupations'].get(user_data[self.menu_name]['occupation'], None)

    def query_object(self, context):
        return DBSession.query(User).get(context.user_data['user'].id)

    def fields(self, context):
        user_data = context.user_data
        user = context.user_data['user']
        _ = context.user_data['_']

        return [self.Field('desired_name', _("1.Name*"), validator=validators.String(), required=True),
                self.Field('region', _("2.Region*"), validator=validators.String(), required=True, variants=[emojize(':globe_with_meridians: ') + _("Non-resident")] + sorted(list(user_data[self.menu_name]['regions'].values()))[:-1]),
                self.Field('phone', _("3.Phone*"), validator=PhoneNumber(), required=True, before_validate=lambda data: data if data.startswith('+') else '+' + data, invalid_message=_("Invalid phone number {emoji}\nPlease enter it in format:\n38XXXXXXXXXX").format(emoji=emojize(':slightly_frowning_face:')) + '\n\n<i>' + _("Example: ") + "380500000000</i>"),
                self.Field('email', _("4.Email"), validator=validators.Email(), invalid_message=_("Invalid Email {emoji}\nPlease try again").format(emoji=emojize(":slightly_frowning_face:"))),
                self.Field('company', _("5.Company"), validator=validators.String()),
                self.Field('occupation', _("6.Activities"), validator=validators.String(), variants=list(user_data[self.menu_name]['occupations'].values()), variants_group_size=1, default=""),
                ]

    def variant_center_buttons(self, context):
        return []

    def entry_points(self):
        return generate_regex_handlers("My profile", self.entry, left_text=emojize(':clipboard: ')) + \
               [CallbackQueryHandler(self.entry, pattern='profile')]

    def message_top_text(self, context):
        _ = context.user_data['_']
        message_text = emojize(":clipboard: ") + "<b>" + _("MY PROFILE") + "</b>\n"
        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'
        return message_text

    def message_bottom_text(self, context):
        _ = context.user_data['_']
        menu_data = context.user_data[self.menu_name]
        active_field = self.active_field(context)
        message_text = emojize(":wavy_dash:") * 10
        message_text += '\n'
        if menu_data['desired_name'] and \
                menu_data['phone'] and \
                menu_data['email'] and \
                menu_data['region']:
            message_text += "<i>** " + _("Consent to processing personal data") + "</i>\n"

        message_text += '\n'

        if active_field.param == 'desired_name':
            message_text += emojize(':writing_hand: <b>') + _("Please enter your Name and Surname and send me") + "</b>\n"
        if active_field.param == 'phone':
            message_text += emojize(':writing_hand: <b>') + _("Please enter your phone number in the 38XXXXXXXXXX format and send it to me") + "</b>\n"
        if active_field.param == 'email':
            message_text += emojize(':writing_hand: <b>') + _("Please enter your Email and send me. It will need to get your tickets") + "</b>\n"
        if active_field.param == 'region':
            message_text += emojize(':ballot_box_with_check: <b>') + _("Please select your region in Ukraine") + "</b>\n"
        if active_field.param == 'company':
            message_text += emojize(':writing_hand: <b>') + _("Enter the name of your company. It will be displayed in your badges & your ticket") + "</b>\n"
        if active_field.param == 'occupation':
            message_text += emojize(':ballot_box_with_check: <b>') + _("Please select your main activity of your company") + "</b>\n"

        return message_text

    def save_button(self, context):
        return None

    def bottom_buttons(self, context):
        _ = context.user_data['_']
        menu_data = context.user_data[self.menu_name]
        if menu_data['desired_name'] and \
                menu_data['phone'] and \
                menu_data['region']:
            return [[InlineKeyboardButton(emojize(':white_heavy_check_mark: ') + _("Finish registration**"), callback_data=f"save_{self.menu_name}")]]

    def reset_button(self, context):
        _ = context.user_data['_']
        return InlineKeyboardButton(emojize(":broom: ") + _("Reset field"), callback_data="reset_field")

    def back_button(self, context):
        _ = context.user_data['_']
        return InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        if self.parent.menu_name == 'start_menu':
            self.remove_interface_markup(context)
            self.parent.send_message(context)
        elif self.parent.menu_name == 'subevents_menu':
            user = context.user_data['user']
            if user.registered:
                self.parent.payment_info(update, context)
            else:
                self.parent.ask_register(update, context)

        return ConversationHandler.END

    def save(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        if not self.check_fields(context):
            self.delete_interface(context)
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please fill required fields."), show_alert=True)
            self.send_message(context)
            return self.States.ACTION
        self.delete_interface(context, 'variations_interface')

        self.update_object(context)
        try:
            self.bot.answer_callback_query(update.callback_query.id, text=_("Your information saved."), show_alert=True)
        except TelegramError:
            self.bot.send_message(chat_id=user.chat_id, text=_("Your information saved."))

        return self.back(update, context)

    def save_object(self, obj, context, session=None):
        user_data = context.user_data
        _ = user_data['_']

        if obj.occupation in user_data[self.menu_name]['occupations'].values():
            for key, value in user_data[self.menu_name]['occupations'].items():
                if obj.occupation == value:
                    obj.occupation = key
                    break

        if obj.region:
            if obj.region == emojize(':globe_with_meridians: ') + _("Non-resident"):
                obj.region = 'non_resident'
            else:
                for key in user_data[self.menu_name]['regions']:
                    if obj.region == user_data[self.menu_name]['regions'][key]:
                        obj.region = key
                        break

        DBSession.add(obj)
        DBSession.commit()

    def goto_language(self, update, context):
        context.update_queue.put(update)
        return ConversationHandler.END

    def additional_states(self):
        return {self.States.ACTION: [MessageHandler(Filters.regex(emojize(":Ukraine:") + emojize(":Russia:") + emojize(":United_Kingdom:")), self.goto_language)]}
