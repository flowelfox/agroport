import datetime
import enum

from botmanlib.menus import BunchListMenu, OneListMenu
from botmanlib.menus.helpers import generate_regex_handlers, to_state
from emoji import emojize
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, TelegramError
from telegram.ext import ConversationHandler, CallbackQueryHandler, MessageHandler, Filters

from src.menus.subevents import SubeventsMenu
from src.models import Category, DBSession, Event, EventTranslation, User
from src.settings import WEBHOOK_ENABLE


class EventCategoriesMenu(BunchListMenu):
    menu_name = "event_categories_menu"
    model = Category
    auto_hide_arrows = True

    def entry(self, update, context):
        self._load(context)
        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('events_menu_access'):
            self.bot.send_message(chat_id=user.chat_id, text=_("You were restricted to use this menu"))
            return ConversationHandler.END

        self.delete_interface(context, interface_name='main')
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        return DBSession.query(Category).join(Category.events).all()

    def entry_points(self):
        return generate_regex_handlers("Events", self.entry, left_text=emojize(":tear-off_calendar: "))

    def message_text(self, context, obj):
        _ = context.user_data['_']

        if obj:
            message_text = emojize(":ballot_box_with_check:") + "<b>" + _("Select category:") + '</b>\n'
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        if max_page > 1:
            return "(" + _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"
        else:
            return ""

    def object_buttons(self, context, objects):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        for obj in objects:
            trans = obj.get_translation(user.language_code)
            buttons.append([InlineKeyboardButton(trans.name, callback_data=f"ecat_{obj.id}")])
        return buttons

    def back_button(self, context):
        _ = context.user_data['_']
        return InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        self.delete_interface(context)
        self.parent.send_message(context)
        return ConversationHandler.END

    def additional_states(self):
        events_menu = EventsMenu(self)
        return {self.States.ACTION: [events_menu.handler]}


class EventsMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1
        MORE = 2

    menu_name = "buy_events_menu"
    model = Event
    disable_web_page_preview = False
    auto_hide_arrows = True

    def entry(self, update, context):
        user = self.prepare_user(User, update, context)
        _ = context.user_data['_']

        data = update.callback_query.data
        if 'goto_event_' in data:
            if not user.desired_name or not user.phone or not user.email or not user.region:
                context.bot.answer_callback_query(update.callback_query.id, text=_("To view this event you need to register"), show_alert=True)
                update.effective_message.edit_reply_markup()
                self.fake_callback_update(user, 'start')
                return ConversationHandler.END

        self._load(context)
        if 'goto_event_' in data:
            self.delete_interface(context)
            update.effective_message.edit_reply_markup()
            context.user_data[self.menu_name]['event_id'] = int(data.replace("goto_event_", ""))
            context.user_data[self.menu_name]['category_id'] = None
        else:
            context.user_data[self.menu_name]['category_id'] = int(data.replace("ecat_", ""))
            context.user_data[self.menu_name]['event_id'] = None
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        if context.user_data[self.menu_name]['event_id']:
            event = DBSession.query(Event).get(context.user_data[self.menu_name]['event_id'])
            return [event]
        else:
            return DBSession.query(Event).join(Event.subevents).filter(Event.end_date > datetime.datetime.now() + datetime.timedelta(hours=1)).filter(Event.category_id == context.user_data[self.menu_name]['category_id']).order_by(Event.start_date).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^ecat_\d+$"),
                CallbackQueryHandler(self.entry, pattern="^goto_event_\d+$")]

    def back_button(self, context):
        _ = context.user_data['_']
        if context.user_data[self.menu_name].get('event_id', False):
            return InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data=f"start")
        else:
            return InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data=f"back_{self.menu_name}")

    def back(self, update, context):
        context.user_data[self.menu_name]['event_id'] = None
        update_objects = getattr(self.parent, 'update_objects', None)
        if update_objects:
            update_objects(context)

        self.parent.send_message(context)
        return ConversationHandler.END

    def center_buttons(self, context, obj=None):
        _ = context.user_data['_']
        buttons = []
        current_object = self.selected_object(context)
        if current_object and current_object.id not in [ticket.event.id for ticket in context.user_data['user'].active_tickets()] and not current_object.sold_out:
            buttons.append(InlineKeyboardButton(emojize(":admission_tickets: ") + _("Ticket"), callback_data='buy_event'))
        return buttons

    def object_buttons(self, context, objects):
        _ = context.user_data['_']
        buttons = []
        if objects:
            buttons.append([InlineKeyboardButton(emojize(":information: ") + _("More Info"), callback_data='event_more')])
        return buttons

    def more(self, update, context):
        _ = context.user_data['_']
        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_events')]]

        translation = self.selected_object(context).get_translation(context.user_data['user'].language_code)
        if translation is None:
            translation = EventTranslation(name=_("Unknown"), description=_("Unknown"))

        message_text = "<b>" + (translation.name if translation.name else _("Unknown")) + "</b>\n"
        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'
        message_text += "<i>" + (translation.description if translation.description else _("Unknown")) + "</i>"
        self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.MORE

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        if obj:
            translation = obj.get_translation(user.language_code)
            message_text = f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += f"<b>{(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            message_text += f"<i>{(translation.description[:50] + ('...' if len(translation.description) > 50 else '') if translation.description else _('Unknown'))}</i>\n\n"
            message_text += emojize(":eleven_o’clock: ") + "<b>" + _("Start date") + f":</b> {obj.start_date.strftime('%H:%M %d.%m.%Y') if obj.start_date else _('Unknown')}\n"
            message_text += emojize(":one-thirty: ") + "<b>" + _("End date") + f":</b> {obj.end_date.strftime('%H:%M %d.%m.%Y') if obj.end_date else _('Unknown')}\n\n"
            message_text += emojize(":round_pushpin: ") + "<b>" + _("Venue") + f":</b> {translation.place if translation.place else _('Unknown')}\n"
            message_text += emojize(":bellhop_bell: ") + "<b>" + _("Organizers") + f":</b> {translation.organizers if translation.organizers else _('Unknown')}\n"
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            if self.selected_object(context).id in [ticket.event.id for ticket in context.user_data['user'].active_tickets()]:
                message_text += '\n' + emojize(":white_heavy_check_mark: ") + '<b>' + _("You have already purchased the ticket. You can add events to this ticket in the section {emoji} My Tickets").format(emoji=emojize(":admission_tickets:")) + '</b>\n'
        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def send_message(self, context):
        user_data = context.user_data
        user = user_data['user']

        back_button = self.back_button(context)

        if user_data[self.menu_name]['filtered_objects']:
            back_button = self.search_back_button(context)

        objects = self._get_objects(context)

        buttons = []
        before_buttons = self.before_buttons(context)
        if before_buttons:
            buttons.extend(before_buttons)

        if not objects:
            message_text = self.message_text(context, None)
            controls = self.controls(context)
            if controls:
                buttons.append(controls)

            object_buttons = self.object_buttons(context, [])
            if object_buttons:
                buttons.extend(object_buttons)
        else:
            selected_object = objects[user_data[self.menu_name]['selected_object']]

            message_text = self.message_text(context, selected_object)

            controls = self.controls(context, obj=selected_object)
            if controls:
                buttons.append(controls)

            object_buttons = self.object_buttons(context, selected_object)
            if object_buttons:
                buttons.extend(object_buttons)

            delete_button = self.delete_button(context)
            if self.add_delete_button and delete_button:
                buttons.append([delete_button])

            message_text += self.page_text(user_data[self.menu_name]['selected_object'] + 1, user_data[self.menu_name]['max_page'], context)

        if back_button:
            buttons.append([back_button])

        after_buttons = self.after_buttons(context)
        if after_buttons:
            buttons.extend(after_buttons)

        markup = InlineKeyboardMarkup(buttons)

        try:
            if context.user_data[self.menu_name]['event_id']:
                return self.send_or_edit(context, interface_name='distribution_answer', chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)
            else:
                return self.send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=self.parse_mode, disable_web_page_preview=self.disable_web_page_preview)
        except TelegramError as e:
            if e.message.startswith("Can't parse entities"):
                message_text = message_text.replace('\\', "")
                return self.send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=markup, parse_mode=None, disable_web_page_preview=self.disable_web_page_preview)
            else:
                raise e

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "(" + _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

    def back_to_event(self, update, context):
        self.send_message(context)
        return self.States.ACTION

    def additional_states(self):
        subevents_menu = SubeventsMenu(self)
        return {self.States.ACTION: [subevents_menu.handler,
                                     CallbackQueryHandler(self.more, pattern='^event_more')],
                self.States.MORE: [CallbackQueryHandler(self.back_to_event, pattern='^back_to_events$'),
                                   MessageHandler(Filters.all, to_state(self.States.MORE))]}
