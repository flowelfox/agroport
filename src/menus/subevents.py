import enum

from botmanlib.menus import OneListMenu
from botmanlib.menus.helpers import to_state, add_to_db, make_payment, check_payment
from emoji import emojize
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update, Chat, User as TUser, CallbackQuery
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters, ConversationHandler

from src.menus.profile import ProfileMenu
from src.models import DBSession, Subevent, Ticket, SubeventTranslation
from src.settings import WEBHOOK_ENABLE


class SubeventsMenu(OneListMenu):
    class States(enum.Enum):
        ACTION = 1
        MORE = 2
        PAYMENT = 3
        PRE_PAYMENT = 4

    menu_name = "subevents_menu"
    model = Subevent
    disable_web_page_preview = False

    def entry(self, update, context):
        self._load(context)

        user = context.user_data['user']
        _ = context.user_data['_']
        if not user.has_permission('buy_ticket_menu_access'):
            self.bot.answer_callback_query(update.callback_query.id, text=_("You were restricted to use this menu"), show_alert=True)
            return ConversationHandler.END

        context.user_data[self.menu_name]['cart'] = []
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def query_objects(self, context):
        event = self.parent.selected_object(context)
        return DBSession.query(Subevent).filter(Subevent.event_id == event.id).order_by(Subevent.date).all()

    def entry_points(self):
        return [CallbackQueryHandler(self.entry, pattern="^buy_event$")]

    def object_buttons(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']
        buttons = []
        if obj:
            if obj in context.user_data[self.menu_name]['cart']:
                buttons.append([InlineKeyboardButton(emojize(":cross_mark: ") + _("Remove from ticket"), callback_data='remove_subevent')])
            else:
                if obj.ticket_limit >= 1:
                    buttons.append([InlineKeyboardButton(emojize(":heavy_plus_sign: ") + _("Add"), callback_data='add_subevent')])
            buttons.append([InlineKeyboardButton(emojize(":information: ") + _("More Info"), callback_data='subevent_more')])
            if not user.registered:
                buttons.append([InlineKeyboardButton(emojize(":white_heavy_check_mark: ") + _("Confirmation / Payment"), callback_data='ask_register')])
            else:
                buttons.append([InlineKeyboardButton(emojize(":white_heavy_check_mark: ") + _("Confirmation / Payment"), callback_data='goto_payment')])
        return buttons

    def ask_register(self, update, context):
        user = context.user_data['user']
        _ = context.user_data['_']

        if not context.user_data[self.menu_name]['cart']:
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please select at least one place to visit"))
            return self.States.ACTION

        buttons = [[InlineKeyboardButton(emojize(":clipboard: ") + _("My profile"), callback_data='profile')],
                   [InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_subevents')]]

        message_text = _("To continue you must register.")
        self.send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.PRE_PAYMENT

    def back_to_subevents(self, update, context):
        self.send_message(context)
        if update.callback_query:
            context.bot.answer_callback_query(update.callback_query.id)
        return self.States.ACTION

    def back_button(self, context):
        _ = context.user_data['_']
        return InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data=f"back_{self.menu_name}")

    def more(self, update, context):
        _ = context.user_data['_']
        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_subevents')]]

        translation = self.selected_object(context).get_translation(context.user_data['user'].language_code)
        if translation is None:
            translation = SubeventTranslation(name=_("Unknown"), description=_("Unknown"))

        message_text = "<b>" + (translation.name if translation.name else _("Unknown")) + "</b>\n"
        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'
        message_text += "<i>" + (translation.description if translation.description else _("Unknown")) + "</i>"
        self.send_or_edit(context, chat_id=context.user_data['user'], text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.MORE

    def add_to_cart(self, update, context):
        _ = context.user_data['_']
        context.user_data[self.menu_name]['cart'].append(self.selected_object(context))
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id, text=_("Added to ticket"))
        return self.States.ACTION

    def remove_from_cart(self, update, context):
        _ = context.user_data['_']
        if self.selected_object(context) in context.user_data[self.menu_name]['cart']:
            context.user_data[self.menu_name]['cart'].remove(self.selected_object(context))
        self.send_message(context)
        self.bot.answer_callback_query(update.callback_query.id, text=_("Removed from ticket"))
        return self.States.ACTION

    def payment_info(self, update, context):
        _ = context.user_data['_']
        user = context.user_data['user']

        if not context.user_data[self.menu_name]['cart']:
            self.bot.answer_callback_query(update.callback_query.id, text=_("Please select at least one place to visit"))
            return self.States.ACTION

        event_translation = self.parent.selected_object(context).get_translation(context.user_data['user'].language_code)
        message_text = "<b>" + _("You ordered ticket to {event_name} event.").format(event_name=event_translation.name) + '</b>\n\n'
        message_text += _("You want to visit and added for your ticket:") + '\n'
        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'
        full_price = 0
        for subevent in context.user_data[self.menu_name]['cart']:
            message_text += emojize(":small_blue_diamond: ") + subevent.get_translation(context.user_data['user'].language_code).name + '\n'
            full_price += subevent.price

        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'

        if full_price > 0:
            order_description = _("Ordering ticket to {}").format(event_translation.name)
            res_data = make_payment(full_price * 100, order_description, lang=user.language_code, response_url=self.bot.link)
            if not res_data:
                return self.conv_fallback(context)
            link = res_data['checkout_url']

            message_text += '\n' + _("You must pay by this link:\n")
            message_text += link + '\n'
            message_text += _("Payment will process automatically, it can take 5-10 seconds.")
            buttons = []
            job_context = {'order_id': res_data['order_id'],
                           'link': link,
                           'user_data': context.user_data}

            context.job_queue.run_repeating(self.job_after_pay, interval=5, context=job_context, first=10)

        else:
            message_text += '\n' + emojize(":shopping_cart:") + "<b>" + _("You ticket is free!") + "</b>"
            buttons = [[InlineKeyboardButton(emojize(":white_heavy_check_mark: ") + _("Confirm"), callback_data='confirm_free_ticket')]]
        buttons.append([InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Back"), callback_data='back_to_subevents')])

        self.send_or_edit(context, chat_id=user.chat_id, text=message_text, reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)
        return self.States.PAYMENT

    def _payment_text(self, context):
        _ = context.user_data['_']
        message_text = emojize(":white_heavy_check_mark:")
        message_text += "<b>" + _("Congratulations!\nYour ticket has been successfully created") + '</b>\n\n'
        message_text += _("<i>Now it is available in the {emoji} </i><b>My tickets</b><i> section of the main menu</i>").format(emoji=emojize(":admission_tickets:")) + "\n\n"
        message_text += "<b>" + _("What's next:") + "</b>\n"
        message_text += emojize(":wavy_dash:") * 10
        message_text += '\n'
        message_text += emojize(":printer: ") + _("You can print it or use the electronic version") + "\n"
        message_text += emojize(":bell: ") + _("We'll remind you about the event a few days before the event")
        return message_text

    def confirm_payment(self, update, context):
        _ = context.user_data['_']
        self.create_ticket(context)

        buttons = [[InlineKeyboardButton(emojize(":fast_reverse_button: ") + _("Main menu"), callback_data='start')]]

        self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=self._payment_text(context), reply_markup=InlineKeyboardMarkup(buttons), parse_mode=self.parse_mode)

        return self.States.PAYMENT

    def create_ticket(self, context):
        _ = context.user_data['_']
        ticket = Ticket()
        ticket.user = context.user_data['user']
        ticket.event = self.parent.selected_object(context)
        full_price = 0
        for subevent in context.user_data[self.menu_name]['cart']:
            ticket.subevents.append(subevent)
            full_price += subevent.price
            subevent.ticket_limit -= 1

        ticket.price = full_price
        if not add_to_db(ticket.subevents + [ticket]):
            return self.conv_fallback(context)

        context.user_data[self.menu_name]['cart'] = []

    def job_after_pay(self, context):
        job = context.job
        if job.context is None:
            job.schedule_removal()
            return

        context.user_data = job.context['user_data']
        _ = context.user_data['_']

        try:
            data = check_payment(job.context['order_id'])
        except TimeoutError as e:
            data = None
            self.logger.warning(str(e))

        if not data or data == 'error':
            context.bot.send_message(chat_id=context.user_data['user'].chat_id, text=_("An error has occurred you do not need to confirm payment."))
            job.schedule_removal()

        elif data == 'expired':
            message_text = emojize(":prohibited: ") + _("You have not ordered a ticket") + '\n'
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            message_text += _("Time of payment by the link\n{link}\nwent out.") + '\n\n'
            message_text += _("To get a ticket, go back to the event page and try again for the order and payment")
            context.bot.send_message(chat_id=context.user_data['user'].chat_id, text=message_text.format(link=job.context['link']))
            context.user_data[self.menu_name]['cart'].clear()
            job.schedule_removal()

        elif data == 'success':
            self.create_ticket(context)
            message_text = self._payment_text(context)
            message_text += "\n\n<i>" + _("You will be redirected to main menu after 5 seconds") + "</i>"
            self.send_or_edit(context, chat_id=context.user_data['user'].chat_id, text=message_text, parse_mode=self.parse_mode)
            context.job_queue.run_once(self.goto_start, 5, context=context.user_data, name=f"goto_start_after_{job.context['order_id']}")
            job.schedule_removal()

    def goto_start(self, context):
        context.user_data = context.job.context
        update = Update(0)
        user = context.user_data['user']
        update._effective_user = TUser(user.chat_id, user.first_name, False, user.last_name, user.username, user.language_code, bot=context.bot)
        update._effective_chat = Chat(user.chat_id, Chat.PRIVATE)
        # update.message = Message(0, update.effective_user, datetime.datetime.utcnow(), update.effective_chat, text='/start', bot=context.bot)
        update._effective_message = None
        update.callback_query = CallbackQuery(0, update.effective_user, update.effective_chat, bot=context.bot, data='start')
        context.update_queue.put(update)

    def message_text(self, context, obj):
        user = context.user_data['user']
        _ = context.user_data['_']

        if obj:
            translation = obj.get_translation(user.language_code)

            message_text = f'<a href="{self.bot.get_image_url(translation.image)}">\u200B</a>' if translation.image and WEBHOOK_ENABLE else ""
            message_text += emojize(":heavy_plus_sign: ") + "<i>" + _("Please select events that you want to visit and add its for your Ticket:") + '</i>\n'
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n\n'
            message_text += f"<b>{(translation.name if translation.name else _('Unknown'))}</b>\n"
            message_text += f"<i>{(translation.description[:50] + ('...' if len(translation.description) > 50 else '') if translation.description else _('Unknown'))}</i>\n\n"
            message_text += emojize(":eleven_o’clock: ") + "<b>" + _("Start") + f":</b> {obj.date.strftime('%H:%M %d.%m.%Y') if obj.date else _('Unknown')}\n"
            message_text += emojize(":round_pushpin: ") + "<b>" + _("Venue") + f":</b> {translation.place if translation.place else _('Unknown')}\n"
            message_text += emojize(":bellhop_bell: ") + "<b>" + _("Organizers") + f":</b> {translation.organizers if translation.organizers else _('Unknown')}\n\n"
            message_text += emojize(":shopping_cart:") + "<b>" + _("Price (UAH)") + f":</b> {obj.price if obj.price else _('Free')}\n"
            message_text += emojize(":wavy_dash:") * 10
            message_text += '\n'
            if obj.ticket_limit <= 0:
                message_text += "<i>" + _("Sorry, we already sell all Tickets to this event.") + "</i>\n"

        else:
            message_text = _("There is nothing to list") + '\n'

        return message_text

    def page_text(self, current_page, max_page, context):
        _ = context.user_data['_']
        return "(" + _("Page") + ' ' + str(current_page) + ' ' + _("of") + ' ' + str(max_page) + ")"

    def additional_states(self):
        profile_menu = ProfileMenu(self)

        return {self.States.ACTION: [CallbackQueryHandler(self.more, pattern='^subevent_more$'),
                                     CallbackQueryHandler(self.add_to_cart, pattern='^add_subevent$'),
                                     CallbackQueryHandler(self.remove_from_cart, pattern='^remove_subevent$'),
                                     CallbackQueryHandler(self.payment_info, pattern='^goto_payment$'),
                                     CallbackQueryHandler(self.ask_register, pattern='^ask_register$')],
                self.States.MORE: [CallbackQueryHandler(self.back_to_subevents, pattern='^back_to_subevents$'),
                                   MessageHandler(Filters.all, to_state(self.States.MORE))],
                self.States.PAYMENT: [CallbackQueryHandler(self.back_to_subevents, pattern='^back_to_subevents$'),
                                      CallbackQueryHandler(self.confirm_payment, pattern='^confirm_free_ticket$'),
                                      MessageHandler(Filters.all, to_state(self.States.PAYMENT))],
                self.States.PRE_PAYMENT: [CallbackQueryHandler(self.back_to_subevents, pattern='^back_to_subevents$'),
                                          profile_menu.handler,
                                          MessageHandler(Filters.all, to_state(self.States.PRE_PAYMENT))]
                }
