from src.models import DBSession, Permission, User


# noinspection SpellCheckingInspection
def main():
    """"""

    """-----------------Default-----------------"""
    # menus
    Permission.create('start_menu_access', {'en': 'Access to "Start" menu', 'ru': 'Доступ к меню "Старт"', 'uk': 'Доступ до меню "Старт"'})
    Permission.create('registration_menu_access', {'en': 'Access to "Registration" menu', 'ru': 'Доступ к меню "Регистрация"', 'uk': 'Доступ до меню "Реєстрація"'})
    Permission.create('events_menu_access', {'en': 'Access to "Events" menu', 'ru': 'Доступ к меню "Мероприятия"', 'uk': 'Доступ до меню "Заходи"'})
    Permission.create('my_tickets_menu_access', {'en': 'Access to "My tickets" menu', 'ru': 'Доступ к меню "Мои билеты"', 'uk': 'Доступ до меню "Мої квитки"'})
    Permission.create('ticket_extend_menu_access', {'en': 'Access to "Ticket extend" menu', 'ru': 'Доступ к меню "Управлять билетом"', 'uk': 'Доступ до меню "Керувати квитком"'})
    Permission.create('ticket_return_menu_access', {'en': 'Access to "Ticket return" menu', 'ru': 'Доступ к меню "Верунть билет"', 'uk': 'Доступ до меню "Повернути квиток"'})
    Permission.create('change_language_menu_access', {'en': 'Access to "Change language" menu', 'ru': 'Доступ к меню "Сменить язык"', 'uk': 'Доступ до меню "Змінити мову'})
    Permission.create('buy_ticket_menu_access', {'en': 'Access to "Buy ticket" menu', 'ru': 'Доступ к меню "Покупка билета"', 'uk': 'Доступ до меню "Купити білет"'})

    # actions
    Permission.create('allow_download_ticket', {'en': 'Allow download ticket', 'ru': 'Разрешить скачивать билет', 'uk': 'Дозволити завантажувати квиток'})
    Permission.create('allow_email_ticket', {'en': 'Allow email ticket', 'ru': 'Разрешить отправлять билет на email', 'uk': 'Дозволити надсилати квиток на email'})
    Permission.create('allow_view_ticket_qr_code', {'en': 'Allow view ticket QR code', 'ru': 'Разрешить смотреть QR код билета', 'uk': 'Дозволити переглядати QR код квитка'})

    """-----------------Privileged-----------------"""
    # menus
    Permission.create('admin_menu_access', {'en': 'Access to "Admin" menu', 'ru': 'Доступ к меню "Админ"', 'uk': 'Доступ до меню "Адмін"'})
    Permission.create('permissions_menu_access', {'en': 'Access to "Permissions" menu', 'ru': 'Доступ к меню "Права"', 'uk': 'Доступ до меню "Права"'})
    Permission.create('distribution_menu_access', {'en': 'Access to "Distribution" menu', 'ru': 'Доступ к меню "Рассылка"', 'uk': 'Доступ до меню "Розсилка"'})
    Permission.create('qr_scanner_menu_access', {'en': 'Access to "QR scanner" menu', 'ru': 'Доступ к меню "QR сканнер"', 'uk': 'Доступ до меню "QR сканер"'})
    Permission.create('ticket_returns_menu_access', {'en': 'Access to "Ticket return requests" menu', 'ru': 'Доступ к меню "Запросы на возврат билетов"', 'uk': 'Доступ до меню "Запити на повернення квитків"'})

    Permission.create('users_info_menu_access', {'en': 'Access to "Users info" menu', 'ru': 'Доступ к меню "Информация о пользователях"', 'uk': 'Доступ до меню "Інформація про користувачів"'})
    Permission.create('user_tickets_menu_access', {'en': 'Access to "Tickets info" menu', 'ru': 'Доступ к меню "Информация о билетах"', 'uk': 'Доступ до меню "Інформація про квитки"'})

    # event menu
    Permission.create('admin_events_menu_access', {'en': 'Access to admin "Events" menu', 'ru': 'Доступ к меню "Мероприятия" в админке', 'uk': 'Доступ до меню "Заходи" в адмінці'})
    Permission.create('upload_events_menu_access', {'en': 'Access to "Upload events" menu', 'ru': 'Доступ к меню "Загрузить мероприятия"', 'uk': 'Доступ до меню "Завантажити захід"'})
    Permission.create('add_event_menu_access', {'en': 'Access to "Add event" menu', 'ru': 'Доступ к меню "Добавить мероприятие"', 'uk': 'Доступ до меню "Додати захід"'})
    Permission.create('edit_event_menu_access', {'en': 'Access to "Edit event" menu', 'ru': 'Доступ к меню "Редактировать мероприятие"', 'uk': 'Доступ до меню "Редагувати захід"'})
    Permission.create('event_translations_menu_access', {'en': 'Access to "Event translations" menu', 'ru': 'Доступ к меню "Переводы мероприятий"', 'uk': 'Доступ до меню "Переклади заходів"'})
    Permission.create('add_event_translation_menu_access', {'en': 'Access to "Add event translation" menu', 'ru': 'Доступ к меню "Добавить перевод мероприятия"', 'uk': 'Доступ до меню "Додати переклад заходу"'})
    Permission.create('edit_event_translation_menu_access', {'en': 'Access to "Edit event translation" menu', 'ru': 'Доступ к меню "Редактировать перевод мероприятия"', 'uk': 'Доступ до меню "Редагувати перклад заходу"'})
    Permission.create('allow_delete_event', {'en': 'Allow delete event', 'ru': 'Разрешить удалять мероприятие', 'uk': 'Дозволити видаляти захід'})
    Permission.create('allow_delete_event_translation', {'en': 'Allow delete event translation', 'ru': 'Разрешить удалять перевод мероприятия', 'uk': 'Дозволити видаляти переклад заходу'})

    # subevent menu
    Permission.create('subevents_menu_access', {'en': 'Access to "Subevents" menu', 'ru': 'Доступ к меню "Подмероприятия"', 'uk': 'Доступ до меню "Підзаходи"'})
    Permission.create('upload_subevents_menu_access', {'en': 'Access to "Upload subevents" menu', 'ru': 'Доступ к меню "Загрузить подмероприятия"', 'uk': 'Доступ до меню "Завантажити підзаходи"'})
    Permission.create('add_subevent_menu_access', {'en': 'Access to "Add subevent" menu', 'ru': 'Доступ к меню "Добавить подмероприятие"', 'uk': 'Доступ до меню "Додати підзахід"'})
    Permission.create('edit_subevent_menu_access', {'en': 'Access to "Edit subevent" menu', 'ru': 'Доступ к меню "Редактировать подмероприятие"', 'uk': 'Доступ до меню "Редагувати підзахід"'})
    Permission.create('subevent_translations_menu_access', {'en': 'Access to "Subevent translations" menu', 'ru': 'Доступ к меню "Переводы подмероприятий"', 'uk': 'Доступ до меню "Переклади підзаходів"'})
    Permission.create('add_subevent_translation_menu_access', {'en': 'Access to "Add subevent translation" menu', 'ru': 'Доступ к меню "Добавить перевод подмероприятия"', 'uk': 'Доступ до меню "Додати переклад підзаходу"'})
    Permission.create('edit_subevent_translation_menu_access', {'en': 'Access to "Edit subevent translation" menu', 'ru': 'Доступ к меню "Редактировать перевод подмероприятия"', 'uk': 'Доступ до меню "Редагувати переклад підзаходу"'})
    Permission.create('allow_delete_subevent', {'en': 'Allow delete subevent', 'ru': 'Разрешить удалять подмероприятие', 'uk': 'Дозволити видаляти підзахід'})
    Permission.create('allow_delete_subevent_translation', {'en': 'Allow delete subevent translation', 'ru': 'Разрешить удалять перевод подмероприятия', 'uk': 'Дозволити видаляти переклад підзаходу'})

    # categories menu
    Permission.create('categories_menu_access', {'en': 'Access to "Categories" menu', 'ru': 'Доступ к меню "Категории"', 'uk': 'Доступ до меню "Категорії"'})
    Permission.create('add_category_menu_access', {'en': 'Access to "Add category" menu', 'ru': 'Доступ к меню "Добавить категорию"', 'uk': 'Доступ до меню "Додати категорію"'})
    Permission.create('category_translations_menu_access', {'en': 'Access to "Category translations" menu', 'ru': 'Доступ к меню "Переводы категории"', 'uk': 'Доступ до меню "Переклади категорій"'})
    Permission.create('add_category_translation_menu_access', {'en': 'Access to "Add category translation" menu', 'ru': 'Доступ к меню "Добавить перевод категории"', 'uk': 'Доступ до меню "Додати переклад категорій"'})
    Permission.create('edit_category_translation_menu_access', {'en': 'Access to "Edit category translation" menu', 'ru': 'Доступ к меню "Редактировать перевод категории"', 'uk': 'Доступ до меню "Редагувати переклад категорії"'})
    Permission.create('allow_delete_category', {'en': 'Allow delete category', 'ru': 'Разрешить удалять категорию', 'uk': 'Дозволити видаляти категорію'})
    Permission.create('allow_delete_category_translation', {'en': 'Allow delete category translation', 'ru': 'Разрешить удалять перевод категории', 'uk': 'Дозволити видаляти переклад категорії'})

    # actions
    Permission.create('allow_view_full_qr_info', {'en': 'Allow view full QR info', 'ru': 'Разрешить смотреть полную информацию по QR коду', 'uk': 'Дозволити переглядати повну інформацію по QR коду'})
    Permission.create('allow_view_additional_qr_info', {'en': 'Allow view additional QR info', 'ru': 'Разрешить смотреть дополнительную информацию по QR коду', 'uk': 'Дозволити переглядати додаткову інформацію по QR коду'})
    Permission.create('allow_add_permission', {'en': 'Allow add permission', 'ru': 'Разрешить добавлять право', 'uk': 'Дозволити додавати право'})
    Permission.create('allow_remove_permission', {'en': 'Allow remove permission', 'ru': 'Разрешить удалять право', 'uk': 'Дозволити видаляти право'})
    Permission.create('superuser', {'en': 'Superuser', 'ru': 'Суперпользователь', 'uk': 'Суперкористувач'})

    DBSession.commit()

    superuser = DBSession.query(User).filter(User.username == 'flowelcat').first()
    if superuser:
        for permission in DBSession.query(Permission).all():
            if not superuser.has_permission(permission.code):
                superuser.permissions.append(permission)
        DBSession.add(superuser)
    DBSession.commit()

if __name__ == '__main__':
    main()