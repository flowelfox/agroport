import datetime

from botmanlib.menus.helpers import generate_underscore
from emoji import emojize
from sqlalchemy import Date
from sqlalchemy.orm import Session

from src.models import User, Ticket, Event


def start_week_left_reminder_job(job_queue):
    stop_week_left_reminder_job(job_queue)
    job_queue.run_daily(week_left_reminder_job, time=datetime.time(hour=14, minute=0, second=0), name="week_left_reminder_job")


def stop_week_left_reminder_job(job_queue):
    for job in job_queue.get_jobs_by_name("week_left_reminder_job"):
        job.schedule_removal()


def week_left_reminder_job(context):
    session = Session()

    events_to_remind = session.query(Event) \
        .join(Event.tickets) \
        .join(Ticket.user) \
        .filter(Ticket.returned == False) \
        .filter(Event.start_date.cast(Date) == datetime.date.today() + datetime.timedelta(days=7)) \
        .all()

    for event in events_to_remind:
        tickets_to_remind = [ticket for ticket in event.tickets if ticket.returned == False and ticket.return_request is None and ticket.user is not None]
        for ticket in tickets_to_remind:
            _ = generate_underscore(ticket.user.language_code)
            message_text = emojize(":bell: ") + "<b>" + _("REMINDER") + "</b>\n\n"
            message_text += _("Already in the week {event} will begin. See you!").format(event=ticket.event.get_translation(ticket.user.language_code).name) + '\n\n'
            message_text += "<i>" + _("You can also add events to your ticket and, if necessary, print a PDF") + "</i>"
            context.bot.send_message(chat_id=ticket.user.chat_id, text=message_text, parse_mode="HTML")

    session.close()


def start_day_left_reminder_job(job_queue):
    stop_day_left_reminder_job(job_queue)
    job_queue.run_daily(day_left_reminder_job, time=datetime.time(hour=14, minute=0, second=0), name="day_left_reminder_job")


def stop_day_left_reminder_job(job_queue):
    for job in job_queue.get_jobs_by_name("day_left_reminder_job"):
        job.schedule_removal()


def day_left_reminder_job(context):
    session = Session()

    events_to_remind = session.query(Event) \
        .join(Event.tickets) \
        .join(Ticket.user) \
        .filter(Ticket.returned == False) \
        .filter(Event.start_date.cast(Date) == datetime.date.today() + datetime.timedelta(days=1)) \
        .all()

    for event in events_to_remind:
        tickets_to_remind = [ticket for ticket in event.tickets if ticket.returned == False and ticket.return_request is None and ticket.user is not None]
        for ticket in tickets_to_remind:
            _ = generate_underscore(ticket.user.language_code)
            message_text = emojize(":bell: ") + "<b>" + _("REMINDER") + "</b>\n\n"
            message_text += _("Already tomorrow {event} will begin. See you!").format(event=ticket.event.get_translation(ticket.user.language_code).name) + '\n\n'
            message_text += "<i>" + _("Print a PDF or use a QR Code at check-in") + "</i>"
            context.bot.send_message(chat_id=ticket.user.chat_id, text=message_text, parse_mode="HTML")

    session.close()
