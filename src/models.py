import datetime

from botmanlib.models import DB, User as BUser, Permission as BPermission, users_permissions_table
from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint, DateTime, DDL, event, Table, Float, Boolean
from sqlalchemy.orm import relationship

database = DB()
Base = database.Base

subevents_tickets_asoc_table = Table('subevents_tickets', Base.metadata,
                                     Column('subevent_id', Integer, ForeignKey('subevents.id')),
                                     Column('ticket_id', Integer, ForeignKey('tickets.id'))
                                     )


class User(BUser, Base):
    __tablename__ = 'users'

    permissions_association_table = users_permissions_table(Base.metadata)
    desired_name = Column(String)
    phone = Column(String)
    email = Column(String)
    region = Column(String)
    company = Column(String)
    occupation = Column(String)
    tickets = relationship("Ticket", back_populates='user')
    came_from = Column(String)

    def active_tickets(self):
        return DBSession.query(Ticket).join(Ticket.event).join(Ticket.subevents).filter(Ticket.returned == False).filter(Ticket.user_id == self.id).all()

    def init_permissions(self):
        for permission in ['start_menu_access',
                           'registration_menu_access',
                           'events_menu_access',
                           'my_tickets_menu_access',
                           'ticket_extend_menu_access',
                           'ticket_return_menu_access',
                           'change_language_menu_access',
                           'buy_ticket_menu_access',
                           'allow_download_ticket',
                           'allow_email_ticket',
                           'allow_view_ticket_qr_code']:
            perm = DBSession.query(Permission).get(permission)
            self.permissions.append(perm)

    @property
    def registered(self):
        return self.desired_name and self.phone and self.region


class Permission(BPermission, Base):
    __tablename__ = 'permissions'


class Category(Base):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    translations = relationship("CategoryTranslation", back_populates='category')
    events = relationship("Event", back_populates='category')

    def get_translation(self, lang):
        lang = lang if lang else 'en'
        trans = DBSession.query(CategoryTranslation).filter(CategoryTranslation.category_id == self.id).filter(CategoryTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(CategoryTranslation).filter(CategoryTranslation.category_id == self.id).filter(CategoryTranslation.lang == 'en').first()
        return trans


class CategoryTranslation(Base):
    __tablename__ = 'category_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    category_id = Column(Integer, ForeignKey('categories.id'))
    category = relationship("Category", back_populates='translations')
    name = Column(String)

    __table_args__ = (UniqueConstraint('category_id', 'lang', name='_category_lang_uc'),)


class Event(Base):
    __tablename__ = 'events'

    id = Column(Integer, primary_key=True)
    translations = relationship("EventTranslation", back_populates='event')

    start_date = Column(DateTime, nullable=False)
    end_date = Column(DateTime, nullable=False)
    category_id = Column(Integer, ForeignKey('categories.id'), nullable=False)
    category = relationship("Category", back_populates='events')
    subevents = relationship("Subevent", back_populates='event', cascade='all,delete')
    tickets = relationship("Ticket", back_populates='event', cascade='all,delete')

    def get_translation(self, lang):
        lang = lang if lang else 'en'
        trans = DBSession.query(EventTranslation).filter(EventTranslation.event_id == self.id).filter(EventTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(EventTranslation).filter(EventTranslation.event_id == self.id).filter(EventTranslation.lang == 'en').first()
        return trans

    @property
    def sold_out(self):
        return sum([subevent.ticket_limit for subevent in self.subevents]) <= 0


class EventTranslation(Base):
    __tablename__ = 'event_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    event_id = Column(Integer, ForeignKey('events.id'))
    event = relationship("Event", back_populates='translations')

    name = Column(String)
    description = Column(String)
    place = Column(String)
    image = Column(String)
    organizers = Column(String)

    __table_args__ = (UniqueConstraint('event_id', 'lang', name='_event_lang_uc'),)


class Subevent(Base):
    __tablename__ = 'subevents'

    id = Column(Integer, primary_key=True)
    translations = relationship("SubeventTranslation", back_populates='subevent')

    date = Column(DateTime, nullable=False)
    price = Column(Integer, default=0)
    event_id = Column(Integer, ForeignKey('events.id'), nullable=False)
    event = relationship("Event", back_populates='subevents')
    tickets = relationship("Ticket", secondary=subevents_tickets_asoc_table)
    ticket_limit = Column(Integer, nullable=False)

    def get_translation(self, lang):
        lang = lang if lang else 'en'
        trans = DBSession.query(SubeventTranslation).filter(SubeventTranslation.subevent_id == self.id).filter(SubeventTranslation.lang == lang.lower()).first()
        if trans is None:
            trans = DBSession.query(SubeventTranslation).filter(SubeventTranslation.subevent_id == self.id).filter(SubeventTranslation.lang == 'en').first()
        return trans


class SubeventTranslation(Base):
    __tablename__ = 'subevent_translations'

    id = Column(Integer, primary_key=True)
    lang = Column(String, nullable=False)
    subevent_id = Column(Integer, ForeignKey('subevents.id'))
    subevent = relationship("Subevent", back_populates='translations')

    name = Column(String)
    description = Column(String)
    place = Column(String)
    image = Column(String)
    organizers = Column(String)

    __table_args__ = (UniqueConstraint('subevent_id', 'lang', name='_subevent_lang_uc'),)


class Ticket(Base):
    __tablename__ = 'tickets'

    id = Column(Integer, primary_key=True)
    event_id = Column(Integer, ForeignKey('events.id'), nullable=False)
    event = relationship("Event", back_populates='tickets')

    subevents = relationship("Subevent", secondary=subevents_tickets_asoc_table)
    price = Column(Float, default=0)

    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    user = relationship("User", back_populates='tickets')

    return_request = relationship("TicketReturnRequest", back_populates='ticket', uselist=False)
    returned = Column(Boolean, default=False)

    create_date = Column(DateTime, default=datetime.datetime.now)

    def is_free(self):
        return sum([subevent.price for subevent in self.subevents]) == 0


class TicketReturnRequest(Base):
    __tablename__ = 'ticket_return_requests'

    id = Column(Integer, primary_key=True)
    ticket_id = Column(Integer, ForeignKey('tickets.id'))
    ticket = relationship("Ticket", back_populates='return_request')
    card = Column(String)
    reason = Column(String)
    processed = Column(Boolean, default=False)

    create_date = Column(DateTime, default=datetime.datetime.now)
    processed_date = Column(DateTime)


event.listen(
    Event.__table__,
    "after_create",
    DDL("ALTER SEQUENCE %(table)s_id_seq restart with 10000;")
)

event.listen(
    Subevent.__table__,
    "after_create",
    DDL("ALTER SEQUENCE %(table)s_id_seq restart with 10000;")
)

DBSession = database.DBSession
Base.metadata.create_all(database.engine)
